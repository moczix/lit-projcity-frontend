import * as R from 'rambdax';
import { timer, from, interval, Observable, Subject, merge } from 'rxjs';
import { startWith, mergeMapTo, shareReplay, mergeMap, map, take } from 'rxjs/operators';
import { getUserInfo } from '../core/api/user-auth';

export const refreshUserInfo$: Subject<void> = new Subject<void>();

// prettier-ignore
export const getUserInfo$ = merge(refreshUserInfo$,  interval(30 * 1000))
  .pipe(
    startWith(null), 
    mergeMap(() => from(getUserInfo())), 
    shareReplay(1)
  );

export const getUserId$: Observable<number> = getUserInfo$.pipe(map(R.prop('id')));
export const getUserIdCold$: Observable<number> = getUserInfo$.pipe(map(R.prop('id')), take(1));

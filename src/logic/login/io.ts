import { formScan$, formIsValid$ } from './state-rx';
import { formStateReset, formStateUpdate } from './state-runners';
import { InputChanged } from '@logic/shared/types';

import { formSubmit$, formStateRunner$, formBackendErrorMsg$, destroy$ } from './streams';
import { mergeMapTo, take, filter, switchMap, takeUntil, tap } from 'rxjs/operators';
import { isTruthy, pushPropValueToStream$, pushNullToStream } from '@logic/shared/fp-utils';
import { loginJwt } from '@logic/core/api/token';
import { from } from 'rxjs';
import { tapError } from '@logic/shared/rx-operators';
import { FetchError } from '@logic/core/http-client';
import { dismissError } from '@logic/shared/rx-pipes';
import { destroy } from '@logic/shared/helpers';
import { ApiToken } from '@logic/shared/models/api-token.model';
import { setIsLoggedIn } from '@logic/auth/io';

export const updateForm = (inputUpdate: InputChanged): void => formStateRunner$.next(formStateUpdate(inputUpdate));

export const submitForm = (): void => formSubmit$.next(undefined);

export const onSuccessLogin = (apiToken: ApiToken) => {
  setIsLoggedIn(apiToken);
};

export const controllerIO = (): void => {
  formStateRunner$.next(formStateReset());

  // prettier-ignore
  formSubmit$.pipe(
    mergeMapTo(
      formIsValid$.pipe(take(1))
    ), 
    filter(isTruthy),
    mergeMapTo(formScan$.pipe(take(1))),
    tap(pushNullToStream(formBackendErrorMsg$)),
    switchMap((form: InputChanged) => 
      from(loginJwt(form.email, form.password))
        .pipe(
          tapError<FetchError>(pushPropValueToStream$('message', formBackendErrorMsg$)),
          dismissError(),
        )
    ),
    takeUntil(destroy$)
  )
  .subscribe(onSuccessLogin)
};

export const controllerIOCleanup = (): void => destroy(destroy$);

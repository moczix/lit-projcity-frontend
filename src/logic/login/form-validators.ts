import * as R from 'rambdax';
import { findOr } from '../shared/fp-utils';
import { isNotEmpty, isEmail } from '../shared/validators';
import { gatherFormErrorsCtor } from '@logic/shared/form-common-utils';
import { InputChanged } from '@logic/shared/types';

const runValidator = ([validatorFn, errorMsg]: [() => boolean, string]): null | string =>
  validatorFn() ? null : errorMsg;

const validateEmail = (value: string): string | null =>
  R.piped(
    [
      [(): boolean => isNotEmpty(value), 'this field must be not empty'],
      [(): boolean => isEmail(value), 'this field should be email']
    ],
    R.map(runValidator),
    findOr(Boolean, null)
  );

const validatePassword = (value: string): string | null =>
  R.piped(
    [[(): boolean => isNotEmpty(value), 'this field must be not empty']],
    R.map(runValidator),
    findOr(Boolean, null)
  );

const validateFns = {
  email: validateEmail,
  password: validatePassword
};

export const gatherFormErrors: (form: InputChanged) => InputChanged = gatherFormErrorsCtor(validateFns);

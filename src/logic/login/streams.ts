import { Subject, BehaviorSubject } from 'rxjs';
import { UpdateStateFn, InputChanged } from '@logic/shared/types';

export const formStateRunner$ = new Subject<UpdateStateFn<InputChanged>>();
export const formSubmit$ = new Subject<void>();
export const formBackendErrorMsg$ = new Subject<string>();
export const destroy$ = new Subject<void>();

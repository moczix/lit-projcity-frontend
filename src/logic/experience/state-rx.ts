import { from, interval } from 'rxjs';
import { startWith, mergeMapTo, shareReplay, mergeMap } from 'rxjs/operators';
import { getAllExperience, getUserExperienceAll } from '@logic/core/api/experience';

import { getUserIdCold$ } from '../user/state-rx';

// prettier-ignore
export const getAllExperienceInfo$ = interval(30 * 1000)
  .pipe(
    startWith(null), 
    mergeMap(() => from(getAllExperience())), 
    shareReplay(1)
  );
// prettier-ignore
export const getUserExperienceInfo$ = interval(30 * 1000)
  .pipe(
    startWith(null),
    mergeMapTo(getUserIdCold$),
    mergeMap((userId: number) => from(getUserExperienceAll(userId))), 
  )

export enum ExperienceEventType {
  taskCreated = 'issue_created',
  taskUpdated = 'issue_updated',
  taskClosed = 'issue_closed',
  commentCreated = 'comment_created',
  pullRequestCreated = 'pull_request_created',
  pullRequestClosed = 'pull_request_closed',
  repoPush = 'push'
}

import * as R from 'rambdax';
import { Experience } from '@logic/shared/models/experience.model';
import { ExperienceEventType } from './enums';

export const getExperienceWithEvent = (eventType: ExperienceEventType): ((x: Experience[]) => Experience[]) =>
  R.filter<Experience>(R.pipe(R.prop('event'), R.propEq('value', eventType)));

import { catchError } from 'rxjs/operators';
import { throwError, OperatorFunction } from 'rxjs';

export const tapError = <V>(tap: (error: V) => void): OperatorFunction<unknown, unknown> =>
  catchError((err: V) => {
    tap(err);
    return throwError(err);
  });

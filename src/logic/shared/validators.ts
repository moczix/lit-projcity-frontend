import * as R from 'rambdax';

export const isNotEmpty = (value: string): boolean => R.and(Boolean(value), R.piped(R.isEmpty(value), R.not));

export const isEmail = (value: string): boolean => R.includes('@', value);

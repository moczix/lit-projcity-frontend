export type UpdateStateFn<T> = (acc: T) => T;

export type InputChanged = {
  [fieldName: string]: string;
};

export type InputErrors = InputChanged;

export type ValidateFns = {
  [key: string]: (value: string) => string | null;
};

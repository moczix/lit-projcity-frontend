import { catchError, filter } from 'rxjs/operators';
import { of, pipe, UnaryFunction, Observable } from 'rxjs';

export const dismissError = (): UnaryFunction<Observable<unknown>, Observable<any>> => {
  const filter$ = (): null => null;

  return pipe(
    catchError(() => of(filter$)),
    filter((d: any) => d !== filter$)
  );
};

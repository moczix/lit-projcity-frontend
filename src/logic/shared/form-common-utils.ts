import * as R from 'rambdax';
import { valueByKey, everyIsNull } from './fp-utils';
import { emptySeed } from './helpers';
import { ValidateFns, InputChanged, InputErrors } from './types';

export const validateField = (rules: ValidateFns) => (form: InputChanged, fieldName: string): InputErrors => ({
  [fieldName]: rules[fieldName]((form as any)[fieldName])
});

const reduceErrors = (validateFns: ValidateFns, form: InputChanged) => (acc: InputErrors, key: string): InputErrors =>
  R.merge(acc, validateField(validateFns)(form, key));

// prettier-ignore
export const isFormValid = (formErrors: InputErrors): boolean =>
    R.piped(
      formErrors, 
      R.keys, 
      R.map(valueByKey(formErrors)),
      everyIsNull
    );

export const gatherFormErrorsCtor = (validateFns: ValidateFns) => (form: InputChanged): InputErrors =>
  R.piped(form, R.keys, R.reduce(reduceErrors(validateFns, form), emptySeed()));

import * as R from 'rambdax';
import { Observable, Subject } from 'rxjs';

export const simpleMemo = <T>(initialVal?: T): ((newVal?: T) => T) => {
  let memoVal: T;
  if (initialVal) {
    memoVal = initialVal;
  }
  return (newVal?: T): T => {
    if (typeof newVal !== 'undefined') {
      memoVal = newVal;
    }
    return memoVal;
  };
};

export const mapMemo = <K, V>(): (() => Map<K, V>) => {
  const map: Map<K, V> = new Map();
  return (): Map<K, V> => map;
};

export const memoArray = <T>(): ((newArray?: T[]) => T[]) => {
  let array: T[] = [];
  return (newArray?: T[]): T[] => {
    if (!newArray) {
      return array;
    }
    return (array = newArray);
  };
};

export const last = <T>(arr: T[]): T => R.piped(arr, R.length, (i) => arr[i - 1]);
export const first = <T>(arr: T[]): any => arr?.[0];
export const valueByKey = <T extends object, V>(obj: T) => (key: keyof T): T[keyof T] => obj[key];
export const everyTrue = <T>(arr: T[]): boolean => arr.every(Boolean);
export const everyTrueNot = <T>(arr: T[]): boolean => !arr.every(Boolean);
export const everyIsNotNull = <T>(arr: T[]): boolean => arr.every((i: T) => i !== null);
export const everyIsNull = <T>(arr: T[]): boolean => arr.every((i: T) => i === null);
export const isTruthy = <T>(arg: T): boolean => Boolean(arg);

// prettier-ignore
export function nulledProps<T>(props: string[]): T {
  return R.pipe(
    R.reduce(
      (acc: T, prop: string) => R.assoc<null, T, string>(prop, null, acc), {} as any
    )
  )(props)
}

export const findOr = <T, V>(fn: (item: T) => boolean, fallbackValue: V) => (arrs: T[]): V | NonNullable<T> =>
  R.find(fn, arrs) ?? fallbackValue;

export const argsAt = <V>(index: number) => (...args: any): V => args[index];

export const argsAsArray = <T>(...args: any): any[] => [...args];

export const pushPropValueToStream$ = <T extends object>(prop: keyof T, stream$: Subject<T[keyof T]>) => (
  value: T
): void => stream$.next(value[prop]);

export const pushNullToStream = <T>(stream$: Subject<T>) => (): void => stream$.next(null as any);

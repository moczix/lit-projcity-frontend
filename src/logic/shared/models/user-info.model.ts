export type UserLevelInfo = {
  level: number;
  expToNextLevel: number;
  percent: number;
};

export type UserGroup = {
  id: number;
  name: string;
  companyId: number;
};

export type Company = {
  id: number;
  name: string;
};

export type DistrictInfo = {
  id: number;
  population: number;
  occupiedHouses: number;
  maxOccupiedHouses: number;
  allJobs: number;
  takenJobs: number;
  mapX: number;
  mapY: number;
};

export type UserMiniInfo = {
  id: number;
  email: string;
  username: string;
};

export type UserInfo = {
  id: number;
  username: string;
  email: string;
  intelligence: number;
  wisdom: number;
  experience: number;
  money: number;
  userLevelInfo: UserLevelInfo;
  avatarImgUrl: string;
  integrationsList: string[];
  integrationsAvailable: Array<{
    name: string;
    url: string;
  }>;
  userGroup: UserGroup | null;
  userAwards: UserAwards[];
  district: DistrictInfo | null;
  company: Company | null;
};

export type UserAwards = {
  id: number;
  title: string;
};

const getUserLevelInfoFromJson = (json: any): UserLevelInfo => ({
  level: json['level'],
  expToNextLevel: json['to_next_level'],
  percent: (json['to_previous_level'] / (json['to_next_level'] + json['to_previous_level'])) * 100
});

export const getUserGroupFromJson = (json: any): UserGroup => ({
  id: json['id'],
  name: json['name'],
  companyId: json['company']
});

const getUserAwardFromJson = (json: any): UserAwards => ({
  id: json['id'],
  title: json['title']
});

const getCompanyFromJson = (json: any): Company => ({
  id: json['id'],
  name: json['name']
});

const getDistrictFromJson = (json: any): DistrictInfo => ({
  id: json['id'],
  population: json['count_population'],
  occupiedHouses: json['count_max_living_spots'] - json['count_free_living_spots'],
  maxOccupiedHouses: json['count_max_living_spots'],
  allJobs: json['count_jobs_spots'],
  takenJobs: json['count_max_jobs_spots'],
  mapX: json['position_X'],
  mapY: json['position_Y']
});

export const getDistrictFromJsonArray = (json: any[]): DistrictInfo[] => json.map(getDistrictFromJson);

const getUserAwardsFromJsonArray = (jsons: any[]): UserAwards[] => jsons?.map(getUserAwardFromJson);

export const getUserInfoFromJson = (json: any): UserInfo => ({
  id: json['id'],
  username: json['user']['username'],
  email: json['user']['email'],
  intelligence: json['intelligence'],
  wisdom: json['wisdom'],
  experience: json['experience'],
  money: json['money'],
  userLevelInfo: getUserLevelInfoFromJson(json['level_info']),
  avatarImgUrl: json['avatar'],
  integrationsList: json['integrations'],
  integrationsAvailable: json['integrations_urls']
    ? Object.keys(json['integrations_urls']).map((key) => ({
        name: key,
        url: json['integrations_urls'][key]
      }))
    : [],
  userGroup: json['group'] ? getUserGroupFromJson(json['group']) : null,
  userAwards: getUserAwardsFromJsonArray(json['awards']),
  district: json['neighborhood'] ? getDistrictFromJson(json['neighborhood']) : null,
  company: json['company'] ? getCompanyFromJson(json['company']) : null
});

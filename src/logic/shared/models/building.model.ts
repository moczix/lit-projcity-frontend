export type Building = {
  id: number;
  mapX: number;
  mapY: number;
  blueprintId: number;
  districtId: number;
};

export const getBuildingFromJson = (json: any): Building => ({
  id: json['id'],
  mapX: json['position_X'],
  mapY: json['position_Y'],
  blueprintId: json['blue_print'],
  districtId: json['neighborhood']
});

export const getBuildingFromJsonArray = (jsons: any[]): Building[] => jsons.map(getBuildingFromJson);

export const getBuildingsDummy = () => [
  {
    id: 1,
    mapX: 0,
    mapY: 0,
    blueprintId: 1
  },
  {
    id: 2,
    mapX: 4,
    mapY: 4,
    blueprintId: 5
  }
];

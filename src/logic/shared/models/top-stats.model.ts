import { UserInfo, UserGroup, getUserGroupFromJson } from './user-info.model';
import * as R from 'rambdax';
import { Statistics, getStatisticsFromJson } from './statistic.model';

export type TopStats = Statistics & {
  userId: number;
};

export type TopStatsWithUser = TopStats & {
  user: {
    id: number;
    username: string;
    avatarImgUrl: string;
    group?: UserGroup;
  };
};

export const getTopStatsFromJson = (json: any): TopStats => ({
  userId: json['id'],
  ...getStatisticsFromJson(json)
});

export const getTopStatsWithUserFromJson = (json: any): TopStatsWithUser =>
  R.merge(getStatisticsFromJson(json), {
    userId: ['id'],
    user: {
      id: json['id'],
      username: json['user']['username'],
      avatarImgUrl: json['avatar'],
      group: json['group'] ? getUserGroupFromJson(json['group']) : null
    }
  });

export const getTopStatsFromJsonArray = (jsons: any[]): TopStats[] => jsons.map(getTopStatsFromJson);
export const getTopStatsWithUserFromJsonArray = (jsons: any[]): TopStatsWithUser[] =>
  jsons.map(getTopStatsWithUserFromJson);

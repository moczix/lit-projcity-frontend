import { UserInfo, getUserInfoFromJson } from './user-info.model';
import { Integration, getIntegrationFromJson } from './experience.model';

export type UserMiniInfo = {
  id: number;
  username: string;
  avatarImgUrl: string;
};

export enum TaskStatusType {
  merged = 4,
  open = 1,
  inProgress = 2,
  closed = 3,
  promoted = 5
}

type TaskStatus = {
  type: TaskStatusType;
  title: string;
  currentUser: UserMiniInfo | null;
  createdAt: Date;
};

type Reporter = UserInfo;
export type Task = {
  id: number;
  key: string;
  title: string;
  comments: number;
  updates: number;
  link: string;
  status: TaskStatus;
  reporter: Reporter;
  priority: string;
  connectedTasksIds: number[];
  integration: Integration;
};

export const getUserMiniInfo = (json: any): UserMiniInfo => ({
  id: json['id'],
  username: json['user']?.['username'],
  avatarImgUrl: json['avatar']
});

const getTaskStatusFromJson = (json: any): TaskStatus => ({
  type: json?.['status']['value'],
  title: json?.['status']['display'],
  currentUser: json?.['avatar'] ? getUserMiniInfo(json['avatar']) : null,
  createdAt: json?.['created'] ? new Date(Date.parse(json['created'])) : new Date()
});

export const getTaskFromJson = (json: any): Task => ({
  id: json['id'],
  key: json['key'],
  title: json['title'],
  comments: Number(json['comments']),
  updates: Number(json['updates']),
  link: json['link'],
  priority: json['priority'],
  status: getTaskStatusFromJson(json['status']),
  reporter: getUserInfoFromJson(json['reporter']),
  connectedTasksIds: json['connected_assignables'],
  integration: getIntegrationFromJson(json['integration'])
});

export const getTaskFromJsonArray = (jsons: any[]): Task[] => jsons.map(getTaskFromJson);

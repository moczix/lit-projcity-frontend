export type BuildingBlueprint = {
  id: number;
  name: string;
  assetUrl: string;
  svgWidth: number;
  svgHeight: number;
  mapSizeX: number;
  mapSizeY: number;
  gridXoffset: number;
  gridYoffset: number;
};

export const getBuldingBlueprintFromJson = (json: any): BuildingBlueprint => ({
  id: json['id'],
  name: json['name'],

  assetUrl: json['image'],
  svgWidth: json['svg_width'],
  svgHeight: json['svg_height'],
  mapSizeX: json['size_X'],
  mapSizeY: json['size_Y'],
  gridXoffset: Number(json['grid_offset_X']),
  gridYoffset: Number(json['grid_offset_Y'])
});

export const getBuildingBlueprintFromJsonArray = (jsons: any[]) => jsons.map(getBuldingBlueprintFromJson);

export const getBuildingBlueprintDummy = (): BuildingBlueprint[] => [
  {
    id: 1,
    name: 'House',
    assetUrl: '/assets/house.svg',
    svgWidth: 10,
    svgHeight: 10,
    mapSizeX: 3,
    mapSizeY: 3,
    gridXoffset: 0,
    gridYoffset: 0
  },
  {
    id: 2,
    name: 'houseWithGarage',
    assetUrl: '/assets/house-with-garage.svg',
    svgWidth: 45,
    svgHeight: 33,
    mapSizeX: 2,
    mapSizeY: 3,
    gridXoffset: 0,
    gridYoffset: 0.5
  },
  {
    id: 3,
    name: 'factory',
    assetUrl: '/assets/factory.svg',
    svgWidth: 62,
    svgHeight: 62,
    mapSizeX: 2,
    mapSizeY: 3,
    gridXoffset: -0.5,
    gridYoffset: 0.5
  },
  {
    id: 4,
    name: 'flat',
    assetUrl: '/assets/flat.svg',
    svgWidth: 13,
    svgHeight: 25,
    mapSizeX: 2,
    mapSizeY: 2,
    gridXoffset: 0,
    gridYoffset: 0
  },
  {
    id: 5,
    name: 'flatBig',
    assetUrl: '/assets/flat-big.svg',
    svgWidth: 17,
    svgHeight: 61,
    mapSizeX: 2,
    mapSizeY: 2,
    gridXoffset: 0,
    gridYoffset: 0
  }
];

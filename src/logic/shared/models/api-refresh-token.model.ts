export type ApiRefreshToken = {
  access: string;
};

export const getApiRefreshTokenFromJson = (json: any): ApiRefreshToken => ({
  access: json['access']
});

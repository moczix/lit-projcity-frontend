export type ApiToken = {
  refresh: string;
  access: string;
};

export const getApiTokenFromJson = (json: any): ApiToken => ({
  refresh: json['refresh'],
  access: json['access']
});

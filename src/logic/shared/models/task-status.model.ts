import { TaskStatusType, getUserMiniInfo, UserMiniInfo } from './task';

export type TaskStatus = {
  id: number;
  createdAt: Date;
  endAt: Date | null;
  user: UserMiniInfo | null;
  taskId: number;
  status: {
    value: TaskStatusType;
    display: string;
  };
};

export const getTaskStatusFromJson = (json: any): TaskStatus => ({
  id: json['id'],
  taskId: json['assignable'],
  createdAt: new Date(Date.parse(json['created'])),
  endAt: json['ended'] ? new Date(Date.parse(json['ended'])) : null,
  user: getUserMiniInfo(json['avatar']),
  status: {
    value: json['status']['value'],
    display: json['status']['display']
  }
});

export const getTaskStatusFromJsonArray = (jsons: any[]): TaskStatus[] => jsons.map(getTaskStatusFromJson);

import { Statistics, getStatisticsFromJson } from './statistic.model';

export type CompanyStatistics = {
  companyId: number;
  companyName: string;
  size: number;
  statistics: Statistics;
};

export const getCompanyStatisticsFromJson = (json: any): CompanyStatistics => ({
  companyId: json['id'],
  companyName: json['name'],
  size: json['size'],
  statistics: getStatisticsFromJson(json)
});

export const getCompanyStatisticsFromJsonArray = (jsons: any[]): CompanyStatistics[] =>
  jsons.map(getCompanyStatisticsFromJson);

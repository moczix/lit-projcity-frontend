export type IntegrationsProfile = {
  id: number;
  webhookId: number | string;
  url: string;
  username: string;
  providerName: string;
  providerValue: string;
};

export const getIntegrationsProfileFromJson = (json: any): IntegrationsProfile => ({
  id: json['id'],
  webhookId: json['external_id'],
  url: json['url'],
  username: json['username'],
  providerName: json['provider']['display'],
  providerValue: json['provider']['value']
});

export const getIntegrationsProfileFromJsonArray = (jsons: any[]): IntegrationsProfile[] =>
  jsons.map(getIntegrationsProfileFromJson);

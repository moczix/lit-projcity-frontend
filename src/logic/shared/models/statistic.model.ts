export type Statistics = {
  experience: number;
  issueCreated: number;
  issueUpdated: number;
  issueClosed: number;
  commentCreated: number;
  repositoryPush: number;
  pullRequestCreated: number;
  pullRequestClosed: number;
  jiraActions: number;
  gitlabActions: number;
  slackActions: number;
};

export const getStatisticsFromJson = (json: any): Statistics => ({
  experience: json['statistics']['experience'],
  issueCreated: json['statistics']['issue_created'],
  issueUpdated: json['statistics']['issue_updated'],
  issueClosed: json['statistics']['issue_closed'],
  commentCreated: json['statistics']['comment_created'],
  repositoryPush: json['statistics']['push'],
  pullRequestCreated: json['statistics']['pull_request_created'],
  pullRequestClosed: json['statistics']['pull_request_closed'],
  jiraActions: json['statistics']['jira_provider_actions'],
  gitlabActions: json['statistics']['gitlab_provider_actions'],
  slackActions: json['statistics']['slack_provider_actions']
});

export type Integration = {
  value: string;
  display: string;
};

type Event = {
  value: string;
  display: string;
};

export type Experience = {
  id: number;
  userId: number;
  amount: number;
  createdAt: Date;
  integration: Integration;
  event: Event;
};

export const getIntegrationFromJson = (json: any): Integration => ({
  value: json['value'],
  display: json['display']
});

const getEventFromJson = (json: any): Event => ({
  value: json['value'],
  display: json['display']
});

export const getExperienceFromJson = (json: any): Experience => ({
  id: json['id'],
  userId: json['avatar'],
  amount: json['amount'],
  createdAt: new Date(Date.parse(json['created'])),
  integration: getIntegrationFromJson(json['integration']),
  event: getEventFromJson(json['event'])
});

export const getExperienceFromJsonArray = (jsons: any[]): Experience[] => jsons.map(getExperienceFromJson);

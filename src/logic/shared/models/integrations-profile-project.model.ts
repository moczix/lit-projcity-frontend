export type IntegrationsProfileProject = {
  externalId: string | number;
  name: string;
};

export const getIntegrationsProfileProjectFromJson = (json: any): IntegrationsProfileProject => ({
  externalId: json['external_id'],
  name: json['name']
});

export const getIntegrationsProfileProjectFromJsonArray = (jsons: any[]): IntegrationsProfileProject[] =>
  jsons.map(getIntegrationsProfileProjectFromJson);

export type AssignablesStatistics = {
  userId: number;
  countTaskOpened: number;
  countTaskInProgress: number;
  countPullRequestOpened: number;
};

export const getAssignablesStatisticsFromJson = (json: any): AssignablesStatistics => ({
  userId: json['id'],
  countTaskOpened: json['count_tasks_opened'],
  countTaskInProgress: json['count_tasks_in_progress'],
  countPullRequestOpened: json['count_prs_opened']
});

export const getAssignablesStatisticsFromJsonArray = (jsons: any[]): AssignablesStatistics[] =>
  jsons.map(getAssignablesStatisticsFromJson);

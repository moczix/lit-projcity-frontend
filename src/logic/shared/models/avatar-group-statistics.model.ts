import { Statistics, getStatisticsFromJson } from './statistic.model';

export type AvatarGroupStatistics = {
  groupId: number;
  companyId: number;
  groupName: string;
  statistics: Statistics;
  size: number;
};

export const getAvatarGroupStatisticsFromJson = (json: any): AvatarGroupStatistics => ({
  groupId: json['id'],
  companyId: json['company'],
  groupName: json['name'],
  statistics: getStatisticsFromJson(json),
  size: json['size']
});

export const getAvatarGroupFromJsonArray = (jsons: any[]): AvatarGroupStatistics[] =>
  jsons.map(getAvatarGroupStatisticsFromJson);

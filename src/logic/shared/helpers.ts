import { Subject } from 'rxjs';

export const emptySeed = (): any => ({} as any);

export const destroy = (stream$: Subject<any>): void => {
  stream$.next(null);
  stream$.complete();
};

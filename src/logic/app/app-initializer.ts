import { checkAuth, setIsNotLoggedIn } from '@logic/auth/io';
import { authIsNotLoggedIn$ } from '@logic/auth/state-rx';
import { filter, take } from 'rxjs/operators';
import { env } from '@env/env';

export const runAppInitializer = (): void => {
  checkAuth();
};

const checkLogoutQueryParam = async (): Promise<void> => {
  const urlParams = new URLSearchParams(window.location.search);
  if (!!urlParams.get('logout')) {
    await setIsNotLoggedIn();
    window.location.href = `${env.homePageUrl}`;
  }
};

export const dashboardInitializer = () => {
  authIsNotLoggedIn$.pipe(filter(Boolean), take(1)).subscribe(() => (window.location.href = env.homePageUrl));
  checkLogoutQueryParam();
};

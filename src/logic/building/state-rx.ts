import { from, Observable, interval } from 'rxjs';
import { shareReplay, startWith, mergeMap } from 'rxjs/operators';
import { getAllBuildings, getBuildingsForDistrict } from '@logic/core/api/building';
import { Building } from '@logic/shared/models/building.model';

export const getAllBuildings$: Observable<Building[]> = interval(30 * 1000).pipe(
  startWith(null),
  mergeMap(() => from(getAllBuildings())),
  shareReplay(1)
);

export const getBuildingsForDistrict$ = (districtId: number): Observable<Building[]> =>
  interval(30 * 1000).pipe(
    startWith(null),
    mergeMap(() => from(getBuildingsForDistrict(districtId))),
    shareReplay(1)
  );

import * as R from 'rambdax';
import { TaskStatusType, Task } from '@logic/shared/models/task';

export const getTasksWithStatus = (status: TaskStatusType): ((x: Task[]) => Task[]) =>
  R.filter<Task>(R.pipe(R.prop('status'), R.propEq('type', status)));

import { from, interval, Observable } from 'rxjs';
import { startWith, mergeMapTo, shareReplay, mergeMap, map } from 'rxjs/operators';
import { getAllIntegrationsTasks, getAllIntegrationsTaskForUser } from '../core/api/integrations';
import { getUserIdCold$ } from '@logic/user/state-rx';
import { Task } from '@logic/shared/models/task';

// prettier-ignore
export const getAllIntegrationsTasks$ = interval(30 * 1000)
  .pipe(
    startWith(null), 
    mergeMap(() => from(getAllIntegrationsTasks())), 
    shareReplay(1)
  );

export const getMyUserAllIntegrationsTasks$: Observable<Task[]> = interval(30 * 1000).pipe(
  startWith(null),
  mergeMapTo(getUserIdCold$),
  mergeMap((userId: number) => from(getAllIntegrationsTaskForUser(userId)))
);

export const getUserAllIntegrationsTask$ = (userId: number): Observable<Task[]> =>
  interval(30 * 1000).pipe(
    startWith(null),
    mergeMap(() => from(getAllIntegrationsTaskForUser(userId)))
  );

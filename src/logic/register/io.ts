import { formScan$, formErrors$, formIsValid$, formErrorsOnSubmitOnce$ } from './state-rx';
import { formStateReset, formStateUpdate } from './state-runners';
import { formSubmit$, formStateRunner$, destroy$, formBackendErrorMsg$, formBackendErrors$ } from './streams';
import { mergeMapTo, take, filter, mergeMap, switchMap, tap, takeUntil } from 'rxjs/operators';
import { isTruthy, pushNullToStream, pushPropValueToStream$ } from '@logic/shared/fp-utils';
import { loginJwt } from '@logic/core/api/token';
import { from } from 'rxjs';
import { tapError } from '@logic/shared/rx-operators';
import { FetchError } from '@logic/core/http-client';
import { dismissError } from '@logic/shared/rx-pipes';
import { InputChanged } from '@logic/shared/types';
import { destroy } from '@logic/shared/helpers';
import { createUser } from '@logic/core/api/user-auth';

export const updateForm = (inputUpdate: InputChanged): void => formStateRunner$.next(formStateUpdate(inputUpdate));

export const submitForm = (): void => formSubmit$.next(undefined);

export const onSuccessLogin = () => {};

export const controllerIO = (): void => {
  formStateRunner$.next(formStateReset());

  formScan$.pipe(takeUntil(destroy$)).subscribe(() => {
    pushNullToStream(formBackendErrorMsg$);
    pushNullToStream(formBackendErrors$);
  });

  // prettier-ignore
  formSubmit$.pipe(
    mergeMapTo(
      formIsValid$.pipe(take(1))
    ), 
    filter(isTruthy),
    mergeMapTo(formScan$.pipe(take(1))),
    tap(pushNullToStream(formBackendErrorMsg$)),
    switchMap((form: InputChanged) => 
      from(createUser(form.username, form.email, form.password))
        .pipe(
          tapError<FetchError>(pushPropValueToStream$('objectWithMessages', formBackendErrors$)),
          tapError<FetchError>(pushPropValueToStream$('message', formBackendErrorMsg$)),
          dismissError(),
        )
    ),
  )
  .subscribe(d => console.log('moge wyslac', d))
};

export const controllerIOCleanup = (): void => destroy(destroy$);

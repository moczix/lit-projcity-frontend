import * as R from 'rambdax';
import { nulledProps } from '@logic/shared/fp-utils';
import { InputChanged } from '@logic/shared/types';

export const formStateUpdate = (curr: InputChanged) => (acc: InputChanged): InputChanged => R.merge(acc, curr);

export const formStateReset = () => (acc: InputChanged): InputChanged => nulledProps(['email', 'password']);

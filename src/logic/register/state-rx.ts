import { Subject, Observable, combineLatest } from 'rxjs';
import { scan, shareReplay, map, tap } from 'rxjs/operators';

import { gatherFormErrors } from './form-validators';
import { UpdateStateFn, InputChanged, InputErrors } from '@logic/shared/types';
import { formStateRunner$, formSubmit$ } from './streams';
import { first } from '@logic/shared/fp-utils';
import { isFormValid } from '@logic/shared/form-common-utils';

export const formScan$: Observable<InputChanged> = formStateRunner$.pipe(
  scan((acc: InputChanged, fn: UpdateStateFn<InputChanged>) => fn(acc), {}),
  shareReplay(1)
);

export const formErrors$: Observable<InputErrors> = formScan$.pipe(map(gatherFormErrors));
export const formErrorsOnSubmitOnce$: Observable<InputErrors> = combineLatest(formErrors$, formSubmit$).pipe(
  map(first)
);
export const formIsValid$: Observable<boolean> = formErrors$.pipe(map(isFormValid));

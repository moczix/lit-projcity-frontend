import { Subject, BehaviorSubject } from 'rxjs';
import { UpdateStateFn, InputChanged, InputErrors } from '@logic/shared/types';

export const formStateRunner$ = new Subject<UpdateStateFn<InputChanged>>();
export const formSubmit$ = new Subject<void>();
export const formBackendErrorMsg$ = new Subject<string>();
export const formBackendErrors$ = new Subject<InputErrors>();
export const destroy$ = new Subject<void>();

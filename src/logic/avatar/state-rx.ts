import * as R from 'rambdax';
import { from, interval, Observable } from 'rxjs';
import { startWith, mergeMap } from 'rxjs/operators';
import { getAvatar } from '@logic/core/api/avatar';
import { UserInfo } from '@logic/shared/models/user-info.model';

export const getAvatarInfo$ = (userId: number): Observable<UserInfo> =>
  interval(30 * 1000).pipe(
    startWith(null),
    mergeMap(() => from(getAvatar(userId)))
  );

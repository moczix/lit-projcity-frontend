import * as R from 'rambdax';
import { getAccessToken } from './db/auth';
import { waitForLoggedIn } from '../auth/promises';

export class FetchError {
  constructor(public readonly error: any, public readonly status: number) {}

  public get detail(): string {
    return this.error?.detail;
  }

  public get nonFieldErrorFirst(): string {
    return this.error?.['non_field_errors']?.[0];
  }

  public get message(): string {
    return this.detail || this.nonFieldErrorFirst;
  }

  public get objectWithMessages(): { [key: string]: string } {
    return R.piped(
      this.error,
      R.keys,
      R.reduce((acc, key: string) => R.merge(acc, { [key]: this.error[key][0] }), {})
    );
  }
}

export const get = async <T>(url: string, mapper: (json: any) => T): Promise<T> => {
  const response = await fetch(url);
  if (response.ok) {
    return mapper(await response.json());
  } else {
    throw new FetchError(await response.json(), response.status);
  }
};

export const post = async <T, V>(url: string, body: T, mapper: (json: any) => V) => {
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(body) // body data type must match "Content-Type" header
  });
  if (response.ok) {
    return mapper(await response.json());
  } else {
    throw new FetchError(await response.json(), response.status);
  }
};

export const authGet = async <T, V>(url: string, mapper: (json: any) => V, extractFromResults?: boolean) => {
  await waitForLoggedIn();
  const response = await fetch(url, {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${await getAccessToken()}`
      // 'Content-Type': 'application/x-www-form-urlencoded',
    }
  });
  if (response.ok) {
    const json = await response.json();
    return mapper(extractFromResults ? json['results'] : json);
  } else {
    throw new FetchError(await response.json(), response.status);
  }
};

export const authPost = async <T, V>(
  url: string,
  data: any,
  mapper: (json: any) => V,
  extractFromResults?: boolean
) => {
  await waitForLoggedIn();
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${await getAccessToken()}`
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: JSON.stringify(data)
  });
  if (response.ok) {
    const json = await response.json();
    return mapper(extractFromResults ? json['results'] : json);
  } else {
    throw new FetchError(await response.json(), response.status);
  }
};

import { ApiToken } from '@logic/shared/models/api-token.model';
import { getDb } from '.';

export const saveApiToken = async (apiToken: ApiToken): Promise<void> => {
  await getDb().auth.clear();
  await getDb().auth.add({
    refresh: apiToken.refresh,
    access: apiToken.access
  });
};

export const clearApiToken = async (): Promise<void> => {
  await getDb().auth.clear();
};

export const getApiToken = async (): Promise<ApiToken | null> => {
  const apiTokens = await getDb().auth.toArray();
  return apiTokens?.length > 0 ? apiTokens[0] : null;
};

export const updateAccessToken = async (apiToken: ApiToken, newAccessToken: string) => {
  await getDb().auth.clear();
  await getDb().auth.add({
    refresh: apiToken.refresh,
    access: newAccessToken
  });
};

export const getAccessToken = async (): Promise<string | null> => {
  const apiTokens = await getDb().auth.toArray();
  return apiTokens?.length > 0 ? apiTokens[0].access : null;
};

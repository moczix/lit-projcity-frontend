import Dexie from 'dexie';

interface AuthTable {
  id?: number;
  refresh: string;
  access: string;
}

export class ProjcityDatabase extends Dexie {
  auth: Dexie.Table<AuthTable, number>;

  constructor() {
    super('projcity');
    this.version(1).stores({
      auth: '++id, refresh, access'
    });
    this.auth = this.table('auth');
  }
}

const _db = (): (() => ProjcityDatabase) => {
  const db: ProjcityDatabase = new ProjcityDatabase();
  return (): ProjcityDatabase => db;
};

export const getDb: () => ProjcityDatabase = _db();

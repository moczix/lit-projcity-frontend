import { authGet } from '../http-client';
import { env } from '@env/env';
import { getExperienceFromJsonArray, Experience } from '@logic/shared/models/experience.model';
import { startOfDay, format } from 'date-fns';

export const getAllExperience = (): Promise<Experience[]> =>
  authGet(`${env.api}/api_v1/experience/`, getExperienceFromJsonArray, true);

export const getUserExperienceAll = (userId: number): Promise<Experience[]> =>
  authGet(`${env.api}/api_v1/experience/?avatar=${userId}&ordering=-created&limit=5`, getExperienceFromJsonArray, true);

export const getUserExperienceBetweenDate = (userId: number, startDate: Date, endDate: Date): Promise<Experience[]> =>
  authGet(
    `${env.api}/api_v1/experience/?avatar=${userId}&created__range=${format(
      startDate,
      'yyyy-MM-dd HH:mm:ss'
    )}, ${format(endDate, 'yyyy-MM-dd HH:mm:ss')}`,
    getExperienceFromJsonArray,
    true
  );

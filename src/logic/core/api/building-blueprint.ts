import { authGet } from '../http-client';
import { env } from '@env/env';
import { getBuildingBlueprintFromJsonArray, BuildingBlueprint } from '@logic/shared/models/building-blueprint.model';

export const getAllBuildingBlueprint = (): Promise<BuildingBlueprint[]> =>
  authGet(`${env.api}/api_v1/building-blueprint/`, getBuildingBlueprintFromJsonArray, true);

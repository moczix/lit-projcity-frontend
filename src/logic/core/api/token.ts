import { post } from '../http-client';
import { env } from '@env/env';
import { getApiTokenFromJson, ApiToken } from '@logic/shared/models/api-token.model';
import { getApiRefreshTokenFromJson, ApiRefreshToken } from '@logic/shared/models/api-refresh-token.model';

export const loginJwt = (email: string, password: string): Promise<ApiToken> => {
  return post(`${env.api}/api/token/`, { email, password }, getApiTokenFromJson);
};

export const refreshJwt = (refreshToken: string): Promise<ApiRefreshToken> => {
  return post(`${env.api}/api/token/refresh/`, { refresh: refreshToken }, getApiRefreshTokenFromJson);
};

import { authGet } from '../http-client';
import { env } from '@env/env';
import { UserInfo, getUserInfoFromJson } from '@logic/shared/models/user-info.model';

export const getAvatar = (userId: number): Promise<UserInfo> =>
  authGet(`${env.api}/api_v1/avatar/${userId}/`, getUserInfoFromJson);

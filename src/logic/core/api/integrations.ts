import { authGet } from '../http-client';
import { env } from '@env/env';
import { getTaskFromJsonArray, Task, TaskStatusType, getTaskFromJson } from '@logic/shared/models/task';
import { getTaskStatusFromJsonArray, TaskStatus } from '@logic/shared/models/task-status.model';
import {
  getIntegrationsProfileFromJsonArray,
  IntegrationsProfile
} from '@logic/shared/models/integrations-profile.model';
import {
  IntegrationsProfileProject,
  getIntegrationsProfileProjectFromJsonArray
} from '@logic/shared/models/integrations-profile-project.model';

export const getAllIntegrationsTasks = (): Promise<Task[]> =>
  authGet(`${env.api}/api_v1/integrations/assignable/?assignable_type=1`, getTaskFromJsonArray, true);

export const getAllIntegrationsPullRequests = (): Promise<Task[]> =>
  authGet(`${env.api}/api_v1/integrations/assignable/?assignable_type=2`, getTaskFromJsonArray, true);

export const getAllIntegrationsTaskForUser = (userId: number): Promise<Task[]> =>
  authGet(
    `${env.api}/api_v1/integrations/assignable/?participated=${userId}&?assignable_type=1`,
    getTaskFromJsonArray,
    true
  );

export const getAllIntegrationsPullRequestsForUser = (userId: number): Promise<Task[]> =>
  authGet(
    `${env.api}/api_v1/integrations/assignable/?participated=${userId}&?assignable_type=2`,
    getTaskFromJsonArray,
    true
  );

export const getTasksStatusForUser = (userId: number, status: TaskStatusType): Promise<TaskStatus[]> =>
  authGet(
    `${env.api}/api_v1/integrations/assignable-status/?avatar=${userId}&status=${status}`,
    getTaskStatusFromJsonArray,
    true
  );

export const getTaskStatusForTaskId = (taskId: number): Promise<TaskStatus[]> =>
  authGet(`${env.api}/api_v1/integrations/assignable-status/?assignable=${taskId}`, getTaskStatusFromJsonArray, true);

export const getTaskDetails = (taskId: number): Promise<Task> =>
  authGet(`${env.api}/api_v1/integrations/assignable/${taskId}/`, getTaskFromJson);

export const getIntegrationsProfile = (): Promise<IntegrationsProfile[]> =>
  authGet(`${env.api}/api_v1/integrations/integration-profile/`, getIntegrationsProfileFromJsonArray, true);

export const getIntegrationsProfileProjects = (integrationId: number): Promise<IntegrationsProfileProject[]> =>
  authGet(
    `${env.api}/api_v1/integrations/integration-profile/${integrationId}/get_projects/`,
    getIntegrationsProfileProjectFromJsonArray
  );

import { authPost } from '../http-client';
import { env } from '@env/env';

export const joinCompanyByName = (companyName: string): Promise<any> =>
  // eslint-disable-next-line @typescript-eslint/camelcase
  authPost(`${env.api}/api_v1/company/join/`, { name: companyName }, () => null);

export const joinCompanyById = (companyId: number): Promise<any> =>
  // eslint-disable-next-line @typescript-eslint/camelcase
  authPost(`${env.api}/api_v1/company/join/`, { company_id: companyId }, () => null);

export const createCompany = (companyName: string): Promise<any> =>
  authPost(`${env.api}/api_v1/company/`, { name: companyName }, () => null);

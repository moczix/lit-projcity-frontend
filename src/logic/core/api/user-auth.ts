import { post, authGet } from '../http-client';
import { env } from '@env/env';
import { getUserInfoFromJson, UserInfo } from '@logic/shared/models/user-info.model';

export const createUser = (username: string, email: string, password: string): Promise<number> => {
  return post(`${env.api}/api_v1/auth/create-user/`, { username, email, password }, () => 1);
};

export const getUserInfo = (): Promise<UserInfo> => authGet(`${env.api}/api_v1/auth/user/`, getUserInfoFromJson);

import { authGet } from '../http-client';
import { env } from '@env/env';
import { DistrictInfo, getDistrictFromJsonArray } from '@logic/shared/models/user-info.model';

export const getDistricts = (): Promise<DistrictInfo[]> =>
  authGet(`${env.api}/api_v1/neighborhood/`, getDistrictFromJsonArray, true);

import { authGet } from '../http-client';
import { env } from '@env/env.prod';
import { getAvatarGroupFromJsonArray, AvatarGroupStatistics } from '@logic/shared/models/avatar-group-statistics.model';
import { format, startOfDay, endOfDay } from 'date-fns';

export const getAvatarGroupStatistics = (): Promise<AvatarGroupStatistics[]> =>
  authGet(`${env.api}/api_v1/avatar-group-statistics/`, getAvatarGroupFromJsonArray, true);

export const getAvatarGroupStatisticsByRangeDate = (startDate: Date, endDate: Date): Promise<AvatarGroupStatistics[]> =>
  authGet(
    `${env.api}/api_v1/avatar-group-statistics/?statistics_created__gte=${format(
      startOfDay(startDate),
      'yyyy-MM-dd HH:mm:ss'
    )}&statistics_created__lte=${format(endOfDay(endDate), 'yyyy-MM-dd HH:mm:ss')}`,
    getAvatarGroupFromJsonArray,
    true
  );

import { authGet } from '../http-client';
import { env } from '@env/env';
import {
  getTopStatsFromJsonArray,
  TopStats,
  TopStatsWithUser,
  getTopStatsWithUserFromJsonArray
} from '@logic/shared/models/top-stats.model';
import { format, startOfDay, endOfDay } from 'date-fns';

export const getAllTopStats = (): Promise<TopStats[]> =>
  authGet(`${env.api}/api_v1/top/`, getTopStatsFromJsonArray, true);

export const getAllTopStatsWithUser = (): Promise<TopStatsWithUser[]> =>
  authGet(`${env.api}/api_v1/top/`, getTopStatsWithUserFromJsonArray, true);

export const getAllTopStatsWithUserToday = (): Promise<TopStatsWithUser[]> =>
  authGet(
    `${env.api}/api_v1/top/?statistics_created__gte=${format(
      startOfDay(new Date()),
      'yyyy-MM-dd HH:mm:ss'
    )}&statistics_created__lte=${format(endOfDay(new Date()), 'yyyy-MM-dd HH:mm:ss')}`,
    getTopStatsWithUserFromJsonArray,
    true
  );

export const getAllTopStatsWithUserByDateRange = (startDate: Date, endDate: Date): Promise<TopStatsWithUser[]> =>
  authGet(
    `${env.api}/api_v1/top/?statistics_created__gte=${format(
      startOfDay(startDate),
      'yyyy-MM-dd HH:mm:ss'
    )}&statistics_created__lte=${format(endOfDay(endDate), 'yyyy-MM-dd HH:mm:ss')}`,
    getTopStatsWithUserFromJsonArray,
    true
  );

export const getTodayTopStats = (): Promise<TopStats[]> =>
  authGet(
    `${env.api}/api_v1/top/?statistics_created__gte=${format(startOfDay(new Date()), 'yyyy-MM-dd HH:mm:ss')}`,
    getTopStatsFromJsonArray,
    true
  );

export const getTopPullRequestOpenToday = (): Promise<TopStats[]> =>
  authGet(
    `${env.api}/api_v1/top/?statistics_created__gte=${format(
      startOfDay(new Date()),
      'yyyy-MM-dd HH:mm:ss'
    )}&=statistics_created__lte=${format(
      endOfDay(new Date()),
      'yyyy-MM-dd HH:mm:ss'
    )}&ordering=count_pull_request_created`,
    getTopStatsFromJsonArray,
    true
  );

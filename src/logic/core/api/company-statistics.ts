import { authGet } from '../http-client';
import { env } from '@env/env';
import { getCompanyStatisticsFromJsonArray, CompanyStatistics } from '@logic/shared/models/company-statistics.model';
import { format, startOfDay, endOfDay } from 'date-fns';

export const getCompanyStatistics = (): Promise<CompanyStatistics[]> =>
  authGet(`${env.api}/api_v1/company-statistics/`, getCompanyStatisticsFromJsonArray, true);

export const getCompanyStatisticsByRangeDate = (startDate: Date, endDate: Date): Promise<CompanyStatistics[]> =>
  authGet(
    `${env.api}/api_v1/company-statistics/?statistics_created__gte=${format(
      startOfDay(startDate),
      'yyyy-MM-dd HH:mm:ss'
    )}&statistics_created__lte=${format(endOfDay(endDate), 'yyyy-MM-dd HH:mm:ss')}`,
    getCompanyStatisticsFromJsonArray,
    true
  );

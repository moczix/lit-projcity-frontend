import { authGet } from '../http-client';
import { env } from '@env/env';
import { Building, getBuildingFromJsonArray } from '@logic/shared/models/building.model';

export const getAllBuildings = (): Promise<Building[]> =>
  authGet(`${env.api}/api_v1/building/`, getBuildingFromJsonArray, true);

export const getBuildingsForDistrict = (districtId: number): Promise<Building[]> =>
  authGet(`${env.api}/api_v1/building/?neighborhood=${districtId}`, getBuildingFromJsonArray, true);

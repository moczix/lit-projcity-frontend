import { authGet } from '../http-client';
import { env } from '@env/env.prod';
import { getTaskStatusFromJsonArray } from '@logic/shared/models/task-status.model';
import {
  getAssignablesStatisticsFromJsonArray,
  AssignablesStatistics
} from '@logic/shared/models/assignables-statistics.model';

export const getAssignablesStatisticsOrderByTaskAssigned = (): Promise<AssignablesStatistics[]> =>
  authGet(
    `${env.api}/api_v1/avatar-assignable-statistics/?ordering=count_tasks_opened`,
    getAssignablesStatisticsFromJsonArray,
    true
  );

export const getAssignablesStatisticsOrderByPullRequestOpened = (): Promise<AssignablesStatistics[]> =>
  authGet(
    `${env.api}/api_v1/avatar-assignable-statistics/?ordering=count_prs_opened`,
    getAssignablesStatisticsFromJsonArray,
    true
  );

export const getAssignablesStatisticsOrderByTaskInProgress = (): Promise<AssignablesStatistics[]> =>
  authGet(
    `${env.api}/api_v1/avatar-assignable-statistics/?ordering=count_tasks_in_progress`,
    getAssignablesStatisticsFromJsonArray,
    true
  );

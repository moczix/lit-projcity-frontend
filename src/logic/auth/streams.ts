import { Subject } from 'rxjs';
import { UpdateStateFn } from '@logic/shared/types';
import { AuthStateType } from './enums';

export const authStateRunner$ = new Subject<UpdateStateFn<AuthStateType>>();

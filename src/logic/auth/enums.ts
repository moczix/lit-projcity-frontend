export enum AuthStateType {
  notChecked,
  loggedIn,
  notLoggedIn
}

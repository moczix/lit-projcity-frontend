import { AuthStateType } from './enums';
import { authStateRunner$ } from './streams';
import { changeAuthState } from './state-runners';
import { ApiToken } from '@logic/shared/models/api-token.model';
import { saveApiToken, clearApiToken, getApiToken as getApiTokenFromDb, updateAccessToken } from '../core/db/auth';
import { refreshJwt } from '../core/api/token';

export const setIsLoggedIn = (apiToken: ApiToken): void => {
  saveApiToken(apiToken);
  authStateRunner$.next(changeAuthState(AuthStateType.loggedIn));
};

export const setIsNotLoggedIn = async (): Promise<void> => {
  clearApiToken();
  authStateRunner$.next(changeAuthState(AuthStateType.notLoggedIn));
};

const checkSessionFromUrl = async (): Promise<void> => {
  try {
    const urlParams = new URLSearchParams(window.location.search);
    const refresh = urlParams.get('refresh');
    const access = urlParams.get('access');
    if (refresh && access) {
      const refreshToken = await refreshJwt(refresh);
      await updateAccessToken(
        {
          access,
          refresh
        },
        refreshToken.access
      );
      authStateRunner$.next(changeAuthState(AuthStateType.loggedIn));
      window.history.replaceState(null, null, window.location.pathname);
    } else {
      setIsNotLoggedIn();
    }
  } catch (err) {
    setIsNotLoggedIn();
  }
};

export const checkAuth = async (): Promise<void> => {
  const apiToken = await getApiTokenFromDb();
  try {
    if (apiToken) {
      const refreshToken = await refreshJwt(apiToken.refresh);
      await updateAccessToken(apiToken, refreshToken.access);
      authStateRunner$.next(changeAuthState(AuthStateType.loggedIn));
    } else {
      checkSessionFromUrl();
    }
  } catch (er) {
    checkSessionFromUrl();
  }
};

export const getApiToken: () => Promise<ApiToken | null> = getApiTokenFromDb;

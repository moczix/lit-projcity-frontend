import { Observable } from 'rxjs';
import { AuthStateType } from './enums';
import { authStateRunner$ } from './streams';
import { scan, shareReplay, map, distinctUntilChanged, startWith } from 'rxjs/operators';
import { UpdateStateFn } from '@logic/shared/types';
import { reset } from './state-runners';

export const authState$: Observable<AuthStateType> = authStateRunner$.pipe(
  startWith(reset()),
  scan((acc: AuthStateType, fn: UpdateStateFn<AuthStateType>) => fn(acc), AuthStateType.notChecked),
  shareReplay(1)
);

export const authNotChecked$: Observable<boolean> = authState$.pipe(
  map((state: AuthStateType) => state === AuthStateType.notChecked),
  distinctUntilChanged()
);

export const authIsLoggedIn$: Observable<boolean> = authState$.pipe(
  map((state: AuthStateType) => state === AuthStateType.loggedIn),
  distinctUntilChanged()
);

export const authIsNotLoggedIn$: Observable<boolean> = authState$.pipe(
  map((state: AuthStateType) => state === AuthStateType.notLoggedIn),
  distinctUntilChanged()
);

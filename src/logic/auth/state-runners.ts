import { AuthStateType } from './enums';

export const changeAuthState = (curr: AuthStateType) => (acc: AuthStateType): AuthStateType => curr;

export const reset = () => (acc: AuthStateType): AuthStateType => AuthStateType.notChecked;

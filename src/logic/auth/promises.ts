import * as R from 'rambdax';
import { authIsLoggedIn$ } from './state-rx';
import { filter, take } from 'rxjs/operators';

export const waitForLoggedIn: () => Promise<boolean | undefined> = () =>
  authIsLoggedIn$.pipe(filter(R.equals<boolean>(true)), take(1)).toPromise();

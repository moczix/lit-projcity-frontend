export type TopStatsMyRank = {
  experience: number;
  issueCreated: number;
  issueUpdated: number;
  commentCreated: number;
  repositoryPush: number;
  pullRequestCreated: number;
  jiraActions: number;
  gitlabActions: number;
  slackActions: number;
};

import { from, interval } from 'rxjs';
import { startWith, shareReplay, mergeMap, withLatestFrom, map, tap } from 'rxjs/operators';
import { getUserInfo$ } from '../user/state-rx';
import { findMyStats, calculateMyRankPositionTopStats } from './utils';
import { getAllExperience } from '../core/api/experience';
import { getTodayTopStats } from '../core/api/top-stats';

// prettier-ignore
export const getAllExperienceInfo$ = interval(30 * 1000)
  .pipe(
    startWith(null), 
    mergeMap(() => from(getAllExperience())), 
    shareReplay(1)
  );

// prettier-ignore
export const getTodayTopStats$ = interval(30 * 1000)
  .pipe(
    startWith(null),
    mergeMap(() => from(getTodayTopStats())),
    shareReplay(1)
  )

// prettier-ignore
export const getTodayMyTopStats$ = getTodayTopStats$
  .pipe(
    withLatestFrom(getUserInfo$),
    map(findMyStats),
    shareReplay(1),
  )

export const getMyRankPositionTopStats$ = getTodayTopStats$.pipe(
  withLatestFrom(getUserInfo$),
  map(calculateMyRankPositionTopStats),
  shareReplay(1)
);

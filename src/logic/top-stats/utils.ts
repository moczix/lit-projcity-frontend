import * as R from 'rambdax';
import { TopStats } from '@logic/shared/models/top-stats.model';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { TopStatsMyRank } from './types';

export const findMyStats = ([stats, userInfo]: [TopStats[], UserInfo]): TopStats | undefined =>
  stats.find(R.propEq('userId', userInfo.id));

export const calculateMyRankPositionTopStats = ([stats, userInfo]: [TopStats[], UserInfo]):
  | TopStatsMyRank
  | undefined =>
  R.piped(
    R.dissoc('userId', stats[0]),
    R.keys,
    R.reduce(
      (acc: TopStatsMyRank, key: string) =>
        // prettier-ignore
        R.assoc(
          key, 
          R.piped(
            R.reverse(stats),
            R.sortBy(R.prop(key)), 
            R.reverse,
            R.findIndex(R.propEq('userId', userInfo.id)),
            R.add(1)
          ),
          acc
        ),
      {} as TopStatsMyRank
    )
  );

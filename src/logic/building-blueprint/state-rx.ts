import { from, Observable } from 'rxjs';
import { getAllBuildingBlueprint } from '@logic/core/api/building-blueprint';
import { shareReplay } from 'rxjs/operators';
import { BuildingBlueprint } from '@logic/shared/models/building-blueprint.model';
//import { getBuildingBlueprintDummy } from '@logic/shared/models/building-blueprint.model';

export const getAllBlueprint$: Observable<BuildingBlueprint[]> = from(getAllBuildingBlueprint()).pipe(shareReplay(1));

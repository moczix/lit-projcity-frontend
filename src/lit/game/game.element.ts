import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Subject, Observable } from 'rxjs';
import { destroy } from '@logic/shared/helpers';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { getUserInfo$ } from '@logic/user/state-rx';
import { getAvatarInfo$ } from '@logic/avatar/state-rx';
import { subscribeCtor } from '@helpers/lit-html';

@customElement('pc-game')
export class GameElement extends LitElementShadowless {
  @property({ type: Number }) public userId: number | undefined;

  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  public firstUpdated(): void {
    this._startStage();
  }

  private get _getUserInfoSource$(): Observable<UserInfo> {
    return this.userId ? getAvatarInfo$(this.userId) : getUserInfo$;
  }

  private async _startStage(): Promise<void> {
    const container = this.querySelector('.game-container') as HTMLDivElement;
    const districtStage = await import('./stages/district.stage');
    if (container) {
      districtStage.startDistrictStage(
        container,
        container?.clientWidth,
        container?.clientWidth * 0.65,
        this._getUserInfoSource$,
        this._destroy$
      );
    }
  }

  public disconnectedCallback(): void {
    destroy(this._destroy$);
  }

  public render(): TemplateResult {
    return html`
      <div class="card">
        ${this._subscribe(
          this._getUserInfoSource$,
          (userInfo: UserInfo) => html`
            <div class="card-header">
              <div class="row text-center mt-2">
                <div class="col">
                  <h6 class="mb-1"><i class="fa fa-users text-c-blue"></i>${userInfo.district?.population}</h6>
                  <p class="mb-0">Population</p>
                </div>
                <div class="col">
                  <h6 class="mb-1">
                    <i class="fa fa-home text-c-blue"></i> ${userInfo.district?.occupiedHouses}/${userInfo.district
                      ?.maxOccupiedHouses}
                  </h6>
                  <p class="mb-0">Houses</p>
                </div>
                <div class="col">
                  <h6 class="mb-1">
                    <i class="fa fa-briefcase text-c-blue"></i> ${userInfo.district?.takenJobs}/${userInfo.district
                      ?.allJobs}
                  </h6>
                  <p class="mb-0">Jobs</p>
                </div>
                <div class="col">
                  <h6 class="mb-1"><i class="fa fa-chart-line text-c-blue"></i> +300$</h6>
                  <p class="mb-0">Income per day</p>
                </div>
                <div class="col">
                  <h6 class="mb-1"><i class="fa fa-envelope text-c-blue"></i> 3 days</h6>
                  <p class="mb-0">Major elections</p>
                </div>
                <div class="col">
                  <h6 class="mb-1"><i class="fa fa-hand-holding-usd text-c-blue"></i> 46%</h6>
                  <p class="mb-0">Support</p>
                </div>
                <div class="col">
                  <a href="#">
                    <h6 class="mb-1"><i class="fa fa-city"></i></h6>
                    <p class="mb-0">Go to city</p>
                  </a>
                </div>
              </div>
            </div>
          `
        )}

        <div class="card-body">
          <div class="game-container"></div>
        </div>
      </div>
    `;
  }
}

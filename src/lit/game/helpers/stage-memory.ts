import { startDistrictStage } from '../stages/district.stage';

type StageMemory = {
  containerWidth: number;
  containerHeight: number;
  fixedTileWidth: number;
  fixedTileHeight: number;
};

export enum Stages {
  setupDistrictStage
}

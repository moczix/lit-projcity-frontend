import Konva from 'konva';
import * as R from 'rambdax';
import { createObjType, loadImage } from '../utils/utils';
import { RuntimeSettings, MemoArray, BuildingPlaced } from '../types';
import { getSize, updateDepth } from '../utils/game-utils';
import { GameSettings } from '../settings';
import { drawGrid } from '../utils/grid-drawer';
import { placeBlueprint } from '../utils/place-blueprint';
import { interval, combineLatest, Observable, from } from 'rxjs';
import { getAllBuildings$, getBuildingsForDistrict$ } from '@logic/building/state-rx';
import { Vector2d } from 'konva/types/types';
import { simpleMemo, mapMemo, memoArray, isTruthy } from '@logic/shared/fp-utils';
import { getAllBlueprint$ } from '@logic/building-blueprint/state-rx';
import { take, takeUntil, mergeMap, tap, filter } from 'rxjs/operators';
import { getUserInfo$ } from '@logic/user/state-rx';
import { UserInfo, DistrictInfo } from '@logic/shared/models/user-info.model';
import { getDistricts } from '@logic/core/api/district';
import { drawDistrictBorder } from '../utils/border-drawer';

const runtimeSettings = simpleMemo(createObjType<RuntimeSettings>());
export const preloadedAssets = mapMemo<string, HTMLImageElement>();

const setRuntimeSettings: (newVal?: RuntimeSettings | undefined) => RuntimeSettings = runtimeSettings;
export const getRuntimeSettings: () => RuntimeSettings = runtimeSettings;

function startUpdateLoop(buildingsPlaced: MemoArray<BuildingPlaced>): void {
  updateDepth(buildingsPlaced());
}

function placeBuildings(
  stage: Konva.Stage,
  buildingsLayer: Konva.Layer,
  buildingsPlaced: MemoArray<BuildingPlaced>,
  userInfo$: Observable<UserInfo>,
  destroy$: Observable<void>
): void {
  userInfo$
    .pipe(
      mergeMap(() => userInfo$),
      take(1),
      filter(R.pipe(R.path('district.id'), isTruthy)),
      mergeMap((userInfo: UserInfo) =>
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        combineLatest(getAllBlueprint$, getAllBuildings$, from(getDistricts()))
      ),
      takeUntil(destroy$)
    )
    .subscribe(([blueprints, buildings, districts]) => {
      drawDistrictBorder(stage, districts);
      buildingsPlaced([]);
      buildingsLayer.destroyChildren();

      for (const building of buildings) {
        const district = districts.find((d) => d.id === building.districtId);

        const blueprint = blueprints.find(R.propEq('id', building.blueprintId));
        placeBlueprint(
          buildingsLayer,
          building.id,
          building.mapX + (district?.mapX || 0) * GameSettings.districtSize,
          building.mapY + (district?.mapY || 0) * GameSettings.districtSize,
          blueprint!,
          buildingsPlaced
        );
      }
      updateDepth(buildingsPlaced());
    });
}

function stageZoom(stage: Konva.Stage): void {
  const scaleBy = 1.1;
  stage.on('wheel', (e) => {
    e.evt.preventDefault();

    if (stage.getPointerPosition()) {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const pointerPosition: Vector2d = stage.getPointerPosition()!;
      const oldScale = stage.scaleX();

      const mousePointTo = {
        x: pointerPosition.x / oldScale - stage.x() / oldScale,
        y: pointerPosition.y / oldScale - stage.y() / oldScale
      };

      const newScale = e.evt.deltaY > 0 ? oldScale / scaleBy : oldScale * scaleBy;
      stage.scale({ x: newScale, y: newScale });

      const newPos = {
        x: -(mousePointTo.x - pointerPosition.x / newScale) * newScale,
        y: -(mousePointTo.y - pointerPosition.y / newScale) * newScale
      };
      stage.position(newPos);
      stage.batchDraw();
    }
  });
}

export async function preloadStage(): Promise<void> {
  const blueprints = await getAllBlueprint$.pipe(take(1)).toPromise();
  for (const blueprint of blueprints) {
    preloadedAssets().set(
      blueprint.assetUrl,
      await loadImage(blueprint.assetUrl, getSize(blueprint.svgWidth), getSize(blueprint.svgHeight))
    );
  }
}

async function setupStage(container: HTMLDivElement, width: number, height: number): Promise<Konva.Stage> {
  const stage = new Konva.Stage({
    container, // id of container <div>
    width,
    height,
    draggable: true
  });
  container.style.backgroundColor = '#7ac943';
  stageZoom(stage);
  await preloadStage();
  return stage;
}

export async function startDistrictStage(
  container: HTMLDivElement,
  width: number,
  height: number,
  userInfo$: Observable<UserInfo>,
  destroy$: Observable<void>
): Promise<void> {
  const stage = await setupStage(container, width, height);
  setRuntimeSettings({
    containerHeight: height,
    containerWidth: width,
    fixedTileWidth: getSize(GameSettings.tileWidth),
    fixedTileHeight: getSize(GameSettings.tileHeight)
  });

  const buildingsPlacedMemo: MemoArray<BuildingPlaced> = memoArray();
  buildingsPlacedMemo([]);
  drawGrid(stage);

  const buildingsLayer = new Konva.Layer();
  stage.add(buildingsLayer);

  interval(100)
    .pipe(takeUntil(destroy$))
    .subscribe(() => {
      startUpdateLoop(buildingsPlacedMemo);
      buildingsLayer.draw();
    });

  placeBuildings(stage, buildingsLayer, buildingsPlacedMemo, userInfo$, destroy$);
}

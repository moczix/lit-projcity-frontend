import { GameSettings } from '../settings';
import { BuildingBlueprint } from '@logic/shared/models/building-blueprint.model';
import { mapToScreen } from './iso-calc';
import { BuildingPlaced } from '../types';

export function getSize(size: number): number {
  return size * GameSettings.defaultScale;
}

export function calcScale(blueprint: BuildingBlueprint): number {
  const verticies: Array<{ x: number; y: number }> = [];
  for (let x = 0; x <= blueprint.mapSizeX; x++) {
    for (let z = 0; z <= blueprint.mapSizeY; z++) {
      verticies.push(mapToScreen(x, z));
    }
  }

  const minX = Math.min(...verticies.map((v) => v.x));
  const maxX = Math.max(...verticies.map((v) => v.x));
  return (maxX - minX) / getSize(blueprint.svgWidth);
}

export function updateDepth(buildings: BuildingPlaced[]): void {
  const calcDepth = (building: BuildingPlaced): number => 500 + building.sprite.y() / getSize(GameSettings.tileHeight);
  const sorted = buildings.slice().sort((buildingA, buildingB) => calcDepth(buildingA) - calcDepth(buildingB));
  sorted.forEach((building, index) => building.sprite.zIndex(index));
}

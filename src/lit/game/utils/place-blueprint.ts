import { BuildingPlaced, MemoArray } from '../types';
import { mapToScreen } from './iso-calc';
import Konva from 'konva';
import { calcScale, getSize } from './game-utils';
import { BuildingBlueprint } from '@logic/shared/models/building-blueprint.model';
import { preloadedAssets } from '../stages/district.stage';

export function placeBlueprint(
  layer: Konva.Layer,
  buildingId: number,
  x: number,
  y: number,
  blueprint: BuildingBlueprint,
  buildingsPlaced: MemoArray<BuildingPlaced>
): void {
  const point = mapToScreen(x + blueprint.gridXoffset, y + blueprint.gridYoffset);
  const scale = calcScale(blueprint);
  const sprite: Konva.Image = new Konva.Image({
    image: preloadedAssets().get(blueprint.assetUrl),
    x: point.x,
    y: point.y,
    scale: { x: scale, y: scale },
    offset: { x: getSize(blueprint.svgWidth) * 0.5, y: getSize(blueprint.svgHeight) }
  });
  layer.add(sprite);
  layer.batchDraw();

  buildingsPlaced([
    ...buildingsPlaced(),
    {
      buildingId,
      blueprint: blueprint,
      sprite: sprite,
      mapX: x,
      mapY: y
    }
  ]);
}

import { SimplePoint } from '../types';
import { createObjType } from './utils';
import { getRuntimeSettings } from '../stages/district.stage';

export function mapToScreen(x: number, y: number): SimplePoint {
  const point = createObjType<SimplePoint>();
  const settings = getRuntimeSettings();
  point.x = (x - y) * (settings.fixedTileWidth * 0.47) + settings.containerWidth * 0.5 + 3;
  point.y = -(x + y + 1) * (settings.fixedTileHeight * 0.5) + settings.containerHeight + settings.fixedTileHeight * 0.5;
  return point;
}

export function screenToMap(x: number, y: number): SimplePoint {
  const point = createObjType<SimplePoint>();
  const settings = getRuntimeSettings();

  const fixedX = x - settings.containerWidth * 0.5;
  const fixedY = y - settings.containerHeight;

  point.x = fixedX / settings.fixedTileWidth - fixedY / settings.fixedTileHeight;
  point.y = -(fixedX / settings.fixedTileWidth + fixedY / settings.fixedTileHeight);

  point.x = parseInt(point.x as any, 10);
  point.y = parseInt(point.y as any, 10);
  return point;
}

import { GameSettings } from '../settings';
import { mapToScreen } from './iso-calc';
import { SimplePoint } from '../types';
import Konva from 'konva';

export const drawIsoLineCtor = (layer: Konva.Layer) => (
  startPoint: SimplePoint,
  endPoint: SimplePoint,
  color = 'black',
  strokeWidth = 0.2
): void => {
  const line = new Konva.Line({
    points: [startPoint.x, startPoint.y, endPoint.x, endPoint.y],
    stroke: color,
    strokeWidth
  });
  layer.add(line);
};

export function drawGrid(stage: Konva.Stage): void {
  const layer = new Konva.Layer();

  const drawIsoLine = drawIsoLineCtor(layer);
  for (let x = 0; x < GameSettings.mapSizeX; x++) {
    drawIsoLine(mapToScreen(x, 0), mapToScreen(x, GameSettings.mapSizeX));
  }
  for (let z = 0; z < GameSettings.mapSizeZ; z++) {
    drawIsoLine(mapToScreen(0, z), mapToScreen(GameSettings.mapSizeZ, z));
  }
  stage.add(layer);
}

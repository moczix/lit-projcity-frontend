export function createObjType<T>(): T {
  return {} as T;
}

export function loadImage(imagePath: string, width: number, height: number): Promise<HTMLImageElement> {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.src = imagePath;
    img.width = width;
    img.height = height;
    img.onload = function (): void {
      resolve(img);
    };
    img.onerror = function (err: string | Event): void {
      reject(err);
    };
  });
}

import Konva from 'konva';
import { SimplePoint } from '../types';
import { drawIsoLineCtor } from './grid-drawer';
import { mapToScreen } from './iso-calc';
import { DistrictInfo } from '@logic/shared/models/user-info.model';
import { GameSettings } from '../settings';

function rainbowStop(h) {
  const f = (n, k = (n + h * 12) % 12) => 0.5 - 0.5 * Math.max(Math.min(k - 3, 9 - k, 1), -1);
  const rgb2hex = (r, g, b) =>
    '#' +
    [r, g, b]
      .map((x) =>
        Math.round(x * 255)
          .toString(16)
          .padStart(2, 0)
      )
      .join('');
  return rgb2hex(f(0), f(8), f(4));
}

export const drawDistrictBorder = (stage: Konva.Stage, districts: DistrictInfo[]): void => {
  const layer = new Konva.Layer();

  const drawIsoLine = drawIsoLineCtor(layer);

  for (let i = 0; i < districts.length; i++) {
    const district = districts[i];
    const districtMapX = district.mapX * GameSettings.districtSize;
    const districtMapY = district.mapY * GameSettings.districtSize;

    const color = rainbowStop(0.5 + 0.1 * i);

    drawIsoLine(
      mapToScreen(districtMapX, districtMapY),
      mapToScreen(districtMapX, districtMapY + GameSettings.districtSize),
      color,
      1
    );

    drawIsoLine(
      mapToScreen(districtMapX, districtMapY),
      mapToScreen(districtMapX + GameSettings.districtSize, districtMapY),
      color,
      1
    );

    drawIsoLine(
      mapToScreen(districtMapX + GameSettings.districtSize, districtMapY),
      mapToScreen(districtMapX + GameSettings.districtSize, districtMapY + GameSettings.districtSize),
      color,
      1
    );

    drawIsoLine(
      mapToScreen(districtMapX, districtMapY + GameSettings.districtSize),
      mapToScreen(districtMapX + GameSettings.districtSize, districtMapY + GameSettings.districtSize),
      color,
      1
    );
  }

  stage.add(layer);
};

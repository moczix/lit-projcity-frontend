export enum GameSettings {
  mapSizeX = 1000,
  mapSizeZ = 1000,
  districtSize = 50,
  defaultScale = 2,
  tileWidth = 12.77,
  tileHeight = 7.37
}

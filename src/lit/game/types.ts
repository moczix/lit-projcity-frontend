import { BuildingBlueprint } from '@logic/shared/models/building-blueprint.model';
import { Image } from 'konva/types/shapes/Image';
import Konva from 'konva';

export type MemoArray<T> = (newArray?: T[]) => T[];

export type SimplePoint = {
  x: number;
  y: number;
};

export type RuntimeSettings = {
  containerWidth: number;
  containerHeight: number;
  fixedTileWidth: number;
  fixedTileHeight: number;
};

export type PreloadedBuildingBlueprint = BuildingBlueprint & {
  image: Image;
};

export type BuildingPlaced = {
  buildingId: number;
  blueprint: BuildingBlueprint;
  sprite: Konva.Image;
  mapX: number;
  mapY: number;
};

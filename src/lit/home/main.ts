import '@webcomponents/webcomponentsjs/webcomponents-loader';
import { runAppInitializer } from '@logic/app/app-initializer';

runAppInitializer();

import './elements/lp-user-buttons.element';
import './elements/lp-modal-outlet.element';
import './elements/lp-modal-login.element';
import './elements/lp-modal-register.element';
import './elements/lp-modal-forgot-password.element';
import './elements/lp-newsletter-form.element';
import './elements/lp-error-popup.element';

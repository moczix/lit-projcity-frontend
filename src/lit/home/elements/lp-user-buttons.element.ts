import { TemplateResult, html, customElement } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { litIntl } from '@helpers/lit-intl';
import { urlHash } from '@helpers/lit-utils';
import { subscribeCtor, ifHtml } from '@helpers/lit-html';
import { Subject } from 'rxjs';
import { destroy } from '@logic/shared/helpers';
import { authIsLoggedIn$, authIsNotLoggedIn$ } from '@logic/auth/state-rx';
import { LitDirectivePart } from '@helpers/types';
import { env } from '@env/env';
import { getApiToken, setIsNotLoggedIn } from '@logic/auth/io';

@customElement('lp-user-buttons')
export class LpUserButtonsElement extends LitElementShadowless {
  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  public disconnectedCallback(): void {
    destroy(this._destroy$);
  }

  private _goToDashboard = () => async (): Promise<void> => {
    const apiToken = await getApiToken();
    window.location.href = `${env.dashboardUrl}?access=${apiToken?.access}&refresh=${apiToken?.refresh}`;
  };

  private _goToDashboardNew = () => async (): Promise<void> => {
    const apiToken = await getApiToken();
    window.location.href = `${env.dashboardUrlNew}?access=${apiToken?.access}&refresh=${apiToken?.refresh}`;
  };

  private _logout = () => async (): Promise<void> => {
    await setIsNotLoggedIn();
    window.location.href = `${env.dashboardUrl}?logout=true`;
  };

  private _renderNotLoggedButtons(): LitDirectivePart {
    return this._subscribe(authIsNotLoggedIn$, (isNotLoggedIn: boolean) =>
      ifHtml(
        isNotLoggedIn,
        html`
          <button class="login" @click="${urlHash('modal-login')}">${litIntl('log in')}</button>
          <button class="register" @click="${urlHash('modal-register')}">${litIntl('register')}</button>
          <button class="forgot" @click="${urlHash('modal-forgot-password')}">${litIntl('forgot password?')}</button>
        `
      )
    );
  }

  private _renderLoggedButtons(): LitDirectivePart {
    return this._subscribe(authIsLoggedIn$, (isLoggedIn: boolean) =>
      ifHtml(
        isLoggedIn,
        html`
          <button class="login" @click="${this._goToDashboard()}">${litIntl('dashboard')}</button>
          <button class="login" @click="${this._goToDashboardNew()}">${litIntl('dashboard New')}</button>
          <button class="register" @click="${this._logout()}">${litIntl('logout')}</button>
        `
      )
    );
  }

  public render(): TemplateResult {
    return html` ${this._renderNotLoggedButtons()} ${this._renderLoggedButtons()} `;
  }
}

import { TemplateResult, html, customElement } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('lp-newsletter-form')
export class LpNewsletterFormElement extends LitElementShadowless {
  public render(): TemplateResult {
    return html`
      <form>
        <input name="email" placeholder="e-mail address" type="email" />
        <input id="subscribeTerms" name="terms" type="checkbox" value="true" />
        <label for="subscribeTerms"
          >Phasellus mattis nisl a sollicitudin lacinia. Donec felis nulla, tempus eget odio non, venenatis sodales
          enim. <a href="#">Aenean maximus</a> metus sit amet neque volutpat, in iaculis orci interdum.</label
        >
        <input type="submit" value="get a tour" />
      </form>
    `;
  }
}

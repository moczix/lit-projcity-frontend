import { TemplateResult, html, customElement } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { formErrorsOnSubmitOnce$ } from '@logic/register/state-rx';
import { Subject } from 'rxjs';
import { subscribeCtor, ifHtml } from '@helpers/lit-html';
import { controllerIO, updateForm, submitForm } from '@logic/register/io';
import { controllerIOCleanup } from '@logic/login/io';
import { destroy } from '@logic/shared/helpers';
import { extractValue, epv } from '@helpers/input-util';
import { InputErrors } from '@logic/shared/types';
import { formBackendErrorMsg$, formBackendErrors$ } from '@logic/register/streams';

@customElement('lp-modal-register')
export class LpModalRegisterElement extends LitElementShadowless {
  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  firstUpdated(): void {
    controllerIO();
  }

  disconnectedCallback(): void {
    destroy(this._destroy$);
    controllerIOCleanup();
  }

  private _changeInput = (fieldName: string) => (event: InputEvent): void =>
    updateForm({ [fieldName]: extractValue(event) });

  private _renderInputError = (inputName: string) =>
    this._subscribe(formErrorsOnSubmitOnce$, (error: InputErrors) =>
      ifHtml(
        error[inputName],
        html`
          <span class="error">${error.email}</span>
        `
      )
    );

  private _renderBackendInputError = (inputName: string) =>
    this._subscribe(formBackendErrors$, (error: InputErrors) =>
      ifHtml(
        error[inputName],
        html`
          <span class="error">${error.email}</span>
        `
      )
    );

  private _renderBackendError = () =>
    this._subscribe(formBackendErrorMsg$, (error: string) =>
      ifHtml(
        error,
        html`
          <span class="error">${error}</span>
        `
      )
    );

  public render(): TemplateResult {
    return html`
      <div class="content register show-content">
        <div class="title">register</div>
        ${this._renderBackendError()}
        <form @submit="${epv(submitForm)}">
          <label for="registerUsername"
            >username ${this._renderInputError('username')} ${this._renderBackendInputError('username')}</label
          >
          <input id="registerUsername" name="username" type="text" @input="${this._changeInput('username')}" />
          <label for="registerEmail"
            >e-mail ${this._renderInputError('email')} ${this._renderBackendInputError('email')}</label
          >
          <input id="registerEmail" name="email" type="text" @input="${this._changeInput('email')}" />
          <label for="registerPassword"
            >password ${this._renderInputError('password')} ${this._renderBackendInputError('password')}</label
          >
          <input id="registerPassword" name="password" type="password" @input="${this._changeInput('password')}" />
          <input type="submit" value="register now" />
        </form>
        <div class="social">
          <a href="#"><img alt="google log-ing" src="img/social/google.svg" /> use google</a>
          <a href="#"><img alt="facebook log-ing" src="img/social/facebook.svg" /> use facebook</a>
        </div>
      </div>
    `;
  }
}

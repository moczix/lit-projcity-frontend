import { TemplateResult, html, customElement } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { extractValue, epv } from '@helpers/input-util';
import { ifHtml, subscribe, subscribeCtor } from '@helpers/lit-html';
import { controllerIO, updateForm, submitForm, controllerIOCleanup } from '@logic/login/io';
import { Subject } from 'rxjs';
import { formErrorsOnSubmitOnce$ } from '@logic/login/state-rx';
import { formBackendErrorMsg$ } from '@logic/login/streams';
import { InputErrors } from '@logic/shared/types';
import { destroy } from '@logic/shared/helpers';
@customElement('lp-modal-login')
export class LpModalLoginElement extends LitElementShadowless {
  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  firstUpdated(): void {
    controllerIO();
  }

  disconnectedCallback(): void {
    destroy(this._destroy$);
    controllerIOCleanup();
  }

  private _changeInput = (fieldName: string) => (event: InputEvent): void =>
    updateForm({ [fieldName]: extractValue(event) });

  private _renderInputError = (inputName: string) =>
    this._subscribe(formErrorsOnSubmitOnce$, (error: InputErrors) =>
      ifHtml(error[inputName], html` <span class="error">${error.email}</span> `)
    );

  private _renderBackendError = () =>
    this._subscribe(formBackendErrorMsg$, (error: string) =>
      ifHtml(error, html` <span class="error">${error}</span> `)
    );

  public render(): TemplateResult {
    return html`
      <div class="content login show-content">
        <div class="title">log in</div>
        ${this._renderBackendError()}
        <form @submit="${epv(submitForm)}">
          <label for="loginEmail">email ${this._renderInputError('email')}</label>

          <input id="loginEmail" name="email" type="text" @input="${this._changeInput('email')}" />
          <label for="loginPassword">password ${this._renderInputError('password')} </label>
          <input id="loginPassword" name="password" type="password" @input="${this._changeInput('password')}" />
          <input type="submit" value="login now" />
        </form>
        <div class="social">
          <a href="#"><img alt="google log-ing" src="img/social/google.svg" /> use google</a>
          <a href="#"><img alt="facebook log-ing" src="img/social/facebook.svg" /> use facebook</a>
        </div>
      </div>
    `;
  }
}

import { TemplateResult, html, customElement, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { onHashModalChange, clearUrlHash } from '@helpers/lit-utils';
import { map } from 'rxjs/operators';
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

const modalManifest: { [key: string]: string } = {
  login: 'lp-modal-login',
  register: 'lp-modal-register',
  'forgot-password': 'lp-modal-forgot-password'
};

@customElement('lp-modal-outlet')
export class LpModalOutletElement extends LitElementShadowless {
  @property({ type: String, attribute: false }) private _currentModalTag: string | null = null;

  public firstUpdated(): void {
    onHashModalChange()
      .pipe(map((modalName) => (modalName ? modalManifest[modalName] : null)))
      .subscribe((modalTag) => this._createElementTag(modalTag));
  }

  private _createElementTag(tagName: string | null): void {
    if (tagName) {
      this._currentModalTag = `<${tagName}></${tagName}>`;
    } else {
      this._currentModalTag = null;
    }
  }

  public render(): TemplateResult {
    return html`
      <div class="modal ${this._currentModalTag ? 'show-modal' : ''}">
        ${this._currentModalTag ? unsafeHTML(this._currentModalTag) : ''}
        <div class="close" @click="${clearUrlHash()}"></div>
      </div>
    `;
  }
}

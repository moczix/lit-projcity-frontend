import { customElement, TemplateResult, html } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('lp-error-popup')
export class LpErrorPopupElement extends LitElementShadowless {
  public render(): TemplateResult {
    return html``;
  }
}

import { TemplateResult, html, customElement } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('lp-modal-forgot-password')
export class LpModalForgotPasswordElement extends LitElementShadowless {
  public render(): TemplateResult {
    return html`
      <div class="content forgot show-content">
        <div class="title">reset password</div>
        <form>
          <label for="usernameInput">username </label>
          <input id="usernameInput" name="username" type="text" /> <input type="submit" value="reset now" />
        </form>
      </div>
    `;
  }
}

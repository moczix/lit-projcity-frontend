import { Observable, fromEvent, Subject, combineLatest, merge } from 'rxjs';
import { map, filter, startWith, tap } from 'rxjs/operators';

const clearHash$ = new Subject<null>();

export function urlHash(hash: string): () => void {
  return () => {
    window.location.hash = hash;
  };
}

export function clearUrlHash(): () => void {
  return () => {
    if (window.history) {
      window.history.pushState('', document.title, window.location.href.replace(window.location.hash, ''));
    } else {
      window.location.hash = '';
    }
    clearHash$.next(null);
  };
}

export function onHashChange(): Observable<string> {
  return merge(fromEvent(window, 'hashchange'), clearHash$).pipe(
    startWith(window.location),
    map(() => window.location.hash.replace('#', ''))
  );
}

export function onHashModalChange(): Observable<string | null> {
  return onHashChange().pipe(map((hash) => (hash.includes('modal-') ? hash.replace('modal-', '') : null)));
}

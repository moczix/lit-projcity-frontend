import { directive, TemplateResult } from 'lit-html';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LitElementShadowlessRx } from './lit-element-shadowless';

export const subscribe = directive(
  <T>(stream$: Observable<T>, fn: (data: T) => TemplateResult | string) => (part: any): void => {
    stream$.subscribe((data: T) => {
      part.setValue(fn(data));
      part.commit();
    });
  }
);

export const subscribeCtor = (destroy$: Observable<any>) =>
  directive(<T>(stream$: Observable<T>, fn: (data: T) => TemplateResult | string) => (part: any): void => {
    stream$.pipe(takeUntil(destroy$)).subscribe((data: T) => {
      part.setValue(fn(data));
      part.commit();
    });
  });

export const ifHtml = <T>(condition: any, data: T): T | string => {
  if (condition) {
    return data;
  }
  return '';
};

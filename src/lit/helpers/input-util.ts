export const extractValue = (event: InputEvent): string => (event.target as HTMLInputElement).value;

export const epv = (runFn: () => void) => (event: Event): void => {
  event.preventDefault();
  runFn();
};

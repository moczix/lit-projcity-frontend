import { directive } from 'lit-html';
import { Md5 } from 'ts-md5/dist/md5';

export const litIntl = directive((text: string) => (part: any) => {
  //part.setValue(Md5.hashStr(text));
  part.setValue(text);
});

import { LitElement } from 'lit-element';
import { Subject, Observable } from 'rxjs';
import { destroy } from '@logic/shared/helpers';
import { takeUntil } from 'rxjs/operators';

export class LitElementShadowless extends LitElement {
  createRenderRoot(): this {
    return this;
  }
}

export class LitElementShadowlessRx extends LitElementShadowless {
  private __destroy$ = new Subject<void>();

  protected _runRx(stream$: Observable<any>, prop: keyof this): void {
    stream$.pipe(takeUntil(this.__destroy$)).subscribe((data: any) => {
      this[prop] = data;
      this.requestUpdate();
    });
  }

  protected _runRxFn<V>(stream$: Observable<V>, fn: (data: V) => void) {
    stream$.pipe(takeUntil(this.__destroy$)).subscribe((data: V) => {
      fn(data);
      this.requestUpdate();
    });
  }

  public disconnectedCallback(): void {
    super.disconnectedCallback();
    destroy(this.__destroy$);
  }
}

import { Observable, fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

export const getCurrentRoutePath = (): Observable<string> =>
  fromEvent(window, 'vaadin-router-location-changed').pipe(map((event: any) => event?.detail?.location?.pathname));

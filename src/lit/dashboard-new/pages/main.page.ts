import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-main-page')
export class MainPage extends LitElementShadowless {
  firstUpdated(): void {}

  public render(): TemplateResult {
    return html``;
  }
}

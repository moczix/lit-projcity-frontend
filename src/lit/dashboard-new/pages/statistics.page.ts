import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-statistics-page')
export class RankingPage extends LitElementShadowless {
  firstUpdated(): void {}

  public render(): TemplateResult {
    return html` ranking page `;
  }
}

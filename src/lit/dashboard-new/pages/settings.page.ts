import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-settings-page')
export class SettingsPage extends LitElementShadowless {
  firstUpdated(): void {}

  public render(): TemplateResult {
    return html`
      <div class="dashNoCompany">
        <h2>your are not connected with any company</h2>
        <span>Join existing company or create a new one</span>
        <!-- buttons -->
        <div class="buttons">
          <a class="button default dark" href="#">join company</a>
          <a class="button default light" href="#">create new</a>
        </div>
        <img src="img/dash-no-company.png" alt="no company" />
      </div>
    `;
  }
}

import '@webcomponents/webcomponentsjs/webcomponents-loader';

import { runAppInitializer, dashboardInitializer } from '@logic/app/app-initializer';

runAppInitializer();
dashboardInitializer();

import './dashboard.page';
import './pages/main.page';
import './pages/statistics.page';
import './pages/settings.page';

// ELEMENTS
import './elements/header.element';
import './elements/left-sidebar.element';

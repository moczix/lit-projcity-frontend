import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-header')
export class HeaderElement extends LitElementShadowless {
  public render(): TemplateResult {
    return html`
      <header>
        <a class="logo">
          <img src="img/logo.png" alt="logo" />
        </a>
        <div class="content">
          <!-- logged -->
          <div class="logged">
            <div class="image" style="background:url('img/user.jpg') 50% 50%"></div>
            <div class="lvl">
              <span class="number">4</span>
              <span class="text">level</span>
            </div>
            <div class="info">
              <span class="name">Wojciech Kaczmarczyk</span>
              <span class="position">head of design</span>
            </div>
          </div>
          <!-- menu -->
          <div class="menu">
            <ul>
              <li>
                <a href="#"
                  >messages
                  <span class="count"><span>3</span></span>
                </a>
              </li>
              <li><a href="#">about</a></li>
              <li><a href="#">help</a></li>
              <li class="lang">
                <img class="current" src="img/lang/usa.svg" alt="english language" />
                <div class="change">
                  <ul>
                    <li>
                      <a href="#"><img src="img/lang/poland.svg" alt="english language" /></a>
                    </li>
                    <li>
                      <a href="#"><img src="img/lang/germany.svg" alt="english language" /></a>
                    </li>
                    <li>
                      <a href="#"><img src="img/lang/italy.svg" alt="english language" /></a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </header>
    `;
  }
}

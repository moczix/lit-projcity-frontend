import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Router } from '@vaadin/router';
import { getCurrentRoutePath } from '@helpers/router-utils';
import { setIsNotLoggedIn } from '@logic/auth/io';

@customElement('pc-left-sidebar')
export class LeftSidebarElement extends LitElementShadowless {
  @property({ type: String }) currentRoutePath = '';

  public firstUpdated(): void {
    getCurrentRoutePath().subscribe((path: string) => (this.currentRoutePath = path));
  }

  private _navigateTo = (where: string) => (): boolean => Router.go(where);
  private _isPath = (path: string): boolean => path === this.currentRoutePath;

  private _logout = () => (): void => setIsNotLoggedIn();

  public render(): TemplateResult {
    return html`
      <aside>
        <ul>
          <li class="${this._isPath('/') ? 'active' : ''}" @click="${this._navigateTo('/')}">
            <div class="icon dashboard"></div>
            <span class="title">dashboard</span>
          </li>
          <li class="${this._isPath('/statistics') ? 'active' : ''}" @click="${this._navigateTo('/statistics')}">
            <div class="icon statistics"></div>
            <span class="title">statistics</span>
          </li>
          <li>
            <div class="icon compare"></div>
            <span class="title">compare</span>
          </li>
          <li>
            <div class="icon map"></div>
            <span class="title">map</span>
          </li>
          <li>
            <div class="icon city"></div>
            <span class="title">city</span>
          </li>
          <li>
            <div class="icon user"></div>
            <span class="title">user</span>
          </li>
          <li class="${this._isPath('/settings') ? 'active' : ''}" @click="${this._navigateTo('/settings')}">
            <div class="icon settings"></div>
            <span class="title">settings</span>
          </li>
        </ul>
        <span class="log-out" @click="${this._logout()}">log out</span>
      </aside>
    `;
  }
}

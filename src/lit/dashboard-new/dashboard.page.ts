import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Router } from '@vaadin/router';
import { subscribe, ifHtml } from '@helpers/lit-html';
import { authIsLoggedIn$ } from '@logic/auth/state-rx';
import { filter, take, delay } from 'rxjs/operators';

const _mainRouter = () => {
  let router: Router;
  return (outlet: HTMLElement | null): Router => {
    if (!outlet) {
      return router;
    }
    router = new Router(outlet);
    router.setRoutes([
      { path: '/', component: 'pc-main-page' },
      { path: '/statistics', component: 'pc-statistics-page' },
      { path: '/settings', component: 'pc-settings-page' }
    ]);
    return router;
  };
};
const setupRouter = _mainRouter();

export const getRouter = (): Router => setupRouter(null);

@customElement('pc-dashboard-page')
export class DashboardPage extends LitElementShadowless {
  firstUpdated(): void {
    authIsLoggedIn$
      .pipe(filter(Boolean), take(1), delay(0))
      .subscribe(() => setupRouter(document.getElementById('dashboard-router')));
  }

  public render(): TemplateResult {
    return html`
      ${subscribe(authIsLoggedIn$, (isLoggedIn: boolean) =>
        ifHtml(
          isLoggedIn,
          html`
            <pc-header></pc-header>
            <pc-left-sidebar></pc-left-sidebar>

            <section class="dashboardContent">
              <div id="dashboard-router"></div>
            </section>
          `
        )
      )}
    `;
  }
}

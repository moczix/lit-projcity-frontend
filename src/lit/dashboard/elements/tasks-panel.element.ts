import * as R from 'rambdax';
import { customElement, html, property } from 'lit-element';
import { LitElementShadowlessRx } from '@helpers/lit-element-shadowless';
import { Observable } from 'rxjs';
import {
  getAllIntegrationsTasks$,
  getMyUserAllIntegrationsTasks$,
  getUserAllIntegrationsTask$
} from '@logic/integrations/state-rx';
import { Task, TaskStatusType } from '@logic/shared/models/task';
import { repeat } from 'lit-html/directives/repeat';
import { getUserIdCold$ } from '@logic/user/state-rx';
import { formatDistance } from 'date-fns';
import { DirectiveFn, TemplateResult } from 'lit-html';
import { getTasksWithStatus } from '@logic/integrations/utils';

@customElement('pc-tasks-panel')
export class TasksPanelElement extends LitElementShadowlessRx {
  @property({ type: Number }) public userId: number | undefined;
  @property({ type: Number }) public listLimit = 5;
  @property({ type: Number }) private _activeTab: TaskStatusType = TaskStatusType.open;

  public allTasks: Task[] = [];
  public userTasks: Task[] = [];

  public firstUpdated(): void {
    this._listenRx();
    this._runAwaiter();
  }

  public get currentTasks(): Task[] {
    return getTasksWithStatus(this._activeTab)(this.userTasks);
  }

  private _listenRx(): void {
    this._runRx(getAllIntegrationsTasks$, 'allTasks');
    this._runRx(this._taskSource$, 'userTasks');
  }

  private async _runAwaiter(): Promise<void> {
    if (!this.userId) {
      this.userId = await getUserIdCold$.toPromise();
    }
  }

  private get _taskSource$(): Observable<Task[]> {
    return this.userId ? getUserAllIntegrationsTask$(this.userId) : getMyUserAllIntegrationsTasks$;
  }

  private _renderConnectedTasks(tasks: Task[], connectedTaskIds: number[]): DirectiveFn {
    return repeat(
      connectedTaskIds,
      (_, index) => index,
      (id) => {
        const foundTask = tasks.find(R.propEq('id', id));
        return html`
          <p class="m-0">
            <i class="fab fa-${foundTask?.integration.value}"></i> ${foundTask?.integration.display}<span
              class="text-c-green"
            >
              <a href="${foundTask?.link}">${foundTask?.status.title || 'unassigned'}</a></span
            >
          </p>
        `;
      }
    );
  }

  private _renderTaskRowInfo(allTasks: Task[], tasks: Task[], userId: number | undefined): TemplateResult {
    return html`
      <div class="table-responsive">
        <table class="table table-hover m-0">
          <thead>
            <tr class="text-center">
              <th>ID</th>
              <th>Title</th>
              <th>Status</th>
              <th>Current user</th>
              <th>Days in status</th>
            </tr>
          </thead>
          <tbody class="text-center">
            ${repeat(
              tasks.slice(0, this.listLimit),
              (task) => task.id,
              (task) => html`
                <tr class="${task.status.currentUser?.id === userId ? 'table-warning' : ''}">
                  <td>
                    <a href="/task-details/${task.id}"><i class="fab fa-jira"></i>${task.key}</a>
                  </td>
                  <td>
                    <h6 class="mb-1">
                      <a href="/task-details/${task.id}">${task.key} ${task.title}</a>
                    </h6>
                    <p class="m-0">
                      Reporter<span class="text-c-green">
                        <a href="/statistics/${task.reporter.id}">${task.reporter.username}</a></span
                      >
                    </p>
                  </td>
                  <td class="">
                    <h6 class="mb-1">${task.status.title}</h6>
                    ${this._renderConnectedTasks(allTasks, task.connectedTasksIds)}
                  </td>
                  <td class="text-center">
                    ${task.status.currentUser?.avatarImgUrl
                      ? html`<a href="/statistics/${task.status.currentUser.id}"
                          ><img
                            src="${task.status.currentUser?.avatarImgUrl}"
                            class="img-radius wid-50 align-top m-r-15 bg-c-blue"
                        /></a>`
                      : html``}
                  </td>
                  <td>${formatDistance(task.status.createdAt, new Date())}</td>
                </tr>
              `
            )}
          </tbody>
        </table>
      </div>
    `;
  }

  private _renderViewAllIssues(): TemplateResult {
    if (this.listLimit !== Infinity) {
      return html`<div class="card-footer text-center">
        <a href="#!" class="b-b-primary text-primary">View all issues</a>
      </div>`;
    }
    return html``;
  }

  public render(): TemplateResult {
    return html`
      <div class="card card table-card">
        <div class="card-body">
          <ul class="nav nav-tabs px-3 pt-3 mb-0" id="myTab" role="tablist">
            <li class="nav-item" @click="${() => (this._activeTab = TaskStatusType.inProgress)}">
              <a
                class="nav-link text-uppercase has-ripple ${this._activeTab === TaskStatusType.inProgress
                  ? 'active'
                  : ''}"
                >In progress<span
                  class="ripple ripple-animate"
                  style="height: 120.094px; width: 120.094px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255); opacity: 0.4; top: -40.047px; left: 5.953px;"
                ></span
              ></a>
            </li>
            <li class="nav-item" @click="${() => (this._activeTab = TaskStatusType.open)}">
              <a class="nav-link text-uppercase has-ripple ${this._activeTab === TaskStatusType.open ? 'active' : ''}"
                >Opened<span
                  class="ripple ripple-animate"
                  style="height: 87.6719px; width: 87.6719px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255); opacity: 0.4; top: -23.8359px; left: -15.9297px;"
                ></span
              ></a>
            </li>
            <li class="nav-item" @click="${() => (this._activeTab = TaskStatusType.closed)}">
              <a class="nav-link text-uppercase has-ripple ${this._activeTab === TaskStatusType.closed ? 'active' : ''}"
                >Lately closed<span
                  class="ripple ripple-animate"
                  style="height: 135.094px; width: 135.094px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255); opacity: 0.4; top: -43.547px; left: 17.6874px;"
                ></span
              ></a>
            </li>
            <li class="nav-item" @click="${() => (this._activeTab = TaskStatusType.promoted)}">
              <a
                class="nav-link text-uppercase has-ripple ${this._activeTab === TaskStatusType.promoted
                  ? 'active'
                  : ''}"
                >Promoted<span
                  class="ripple ripple-animate"
                  style="height: 109.281px; width: 109.281px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(70, 128, 255); opacity: 0.4; top: -29.633px; left: -46.4924px;"
                ></span
              ></a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane fade active show">
              ${this._renderTaskRowInfo(this.allTasks, this.currentTasks, this.userId)}
            </div>
          </div>
        </div>
        ${this._renderViewAllIssues()}
      </div>
    `;
  }
}

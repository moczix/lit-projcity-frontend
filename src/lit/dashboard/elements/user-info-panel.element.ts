import { customElement, html, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Subject, Observable } from 'rxjs';
import { repeat } from 'lit-html/directives/repeat';
import { subscribeCtor } from '@helpers/lit-html';
import { destroy } from '@logic/shared/helpers';
import { getUserInfo$ } from '@logic/user/state-rx';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { LitDirectivePart } from '@helpers/types';
import { tap } from 'rxjs/operators';
import { getAvatarInfo$ } from '@logic/avatar/state-rx';

@customElement('pc-user-info-panel')
export class UserInfoPanelElement extends LitElementShadowless {
  @property({ type: Number }) public userId: number | undefined;

  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  public disconnectedCallback(): void {
    destroy(this._destroy$);
  }

  private get _getUserInfoSource$(): Observable<UserInfo> {
    return this.userId ? getAvatarInfo$(this.userId) : getUserInfo$;
  }

  public render(): LitDirectivePart {
    return this._subscribe(
      this._getUserInfoSource$,
      (userInfo: UserInfo) => html`
        <div class="card user-card user-card-3 support-bar1">
          <div class="card-body ">
            <div class="text-center">
              <img class="img-radius img-fluid wid-150 bg-c-blue" src="${userInfo.avatarImgUrl}" alt="User image" />
              <h3 class="mb-1 mt-3 f-w-400">${userInfo.username}</h3>
              <p class="mb-3 text-muted">${userInfo.userLevelInfo.level} Level</p>
              <ul class="list-inline f-20 mt-3 mb-0">
                ${repeat(
                  userInfo.integrationsList,
                  (_, index) => index,
                  (integration) => html`
                    <li class="list-inline-item">
                      <a href="#!" class="text-facebook"><i class="fab fa-${integration}"></i></a>
                    </li>
                  `
                )}
                ${userInfo.userGroup
                  ? html`<li class="list-inline-item">
                      <span class="badge badge-pill badge-primary">${userInfo.userGroup.name}</span>
                    </li>`
                  : html``}
              </ul>
              <div class="alert alert-info m-2">
                <div class="progress">
                  <div
                    class="progress-bar bg-success"
                    role="progressbar"
                    style="width:${userInfo.userLevelInfo.percent}%"
                  ></div>
                </div>
              </div>
              <div class="row text-center">
                <div class="col">
                  <h6 class="mb-1"><i class="fa fa-book text-c-blue"></i> ${userInfo.experience}</h6>
                  <p class="mb-0">Exp</p>
                </div>
                <div class="col">
                  <h6 class="mb-1">
                    <i class="fa fa-arrow-circle-up text-c-blue"></i> ${userInfo.userLevelInfo.expToNextLevel}
                  </h6>
                  <p class="mb-0">Exp to next level</p>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer bg-light">
            <div class="row text-center mt-2">
              <div class="col">
                <h6 class="mb-1"><i class="fa fa-brain text-c-green"></i> ${userInfo.intelligence}</h6>
                <p class="mb-0">Inteligence</p>
              </div>
              <div class="col">
                <h6 class="mb-1"><i class="fa fa-globe text-c-green"></i> ${userInfo.wisdom}</h6>
                <p class="mb-0">Wisdom</p>
              </div>
              <div class="col">
                <h6 class="mb-1"><i class="fa fa-money-bill text-c-green"></i> ${userInfo.money} $</h6>
                <p class="mb-0">Money</p>
              </div>
            </div>
            <hr />
            <h6 class="text-center">Awards <span class="badge badge-success">${userInfo.userAwards.length}</span></h6>
            <div class="row justify-content-md-center text-center">
              ${repeat(
                userInfo.userAwards,
                (award) => award.id,
                (award) => html`
                  <div class="col-md-6 mb-2">
                    <h6 class="mb-1"><i class="fa fa-2x fa-trophy text-c-red"></i></h6>
                    <p class="mb-0">${award.title}</p>
                  </div>
                `
              )}
            </div>
          </div>
        </div>
      `
    );
  }
}

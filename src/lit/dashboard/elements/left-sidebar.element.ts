import { customElement, html, TemplateResult, property } from 'lit-element';
import { styleMap } from 'lit-html/directives/style-map';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Router } from '@vaadin/router';
import { getCurrentRoutePath } from '@helpers/router-utils';
import { setIsNotLoggedIn } from '@logic/auth/io';
import { displayBlock, displayNone } from '@helpers/common-styles';
import { getUserInfo } from '@logic/core/api/user-auth';
import { Subject } from 'rxjs';
import { subscribeCtor } from '@helpers/lit-html';
import { getUserInfo$ } from '@logic/user/state-rx';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { destroy } from '@logic/shared/helpers';
import { delay } from 'rxjs/operators';

@customElement('pc-left-sidebar')
export class LeftSidebarElement extends LitElementShadowless {
  @property({ type: String }) currentRoutePath = '';
  @property({ type: Boolean, attribute: false }) private _userInfoExpanded = false;

  public userHasCompany = false;

  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);
  public firstUpdated(): void {
    getCurrentRoutePath().subscribe((path: string) => (this.currentRoutePath = path));
    this._getUserInfo();
  }

  private _getUserInfo(): void {
    getUserInfo$.pipe(delay(100)).subscribe((userInfo: UserInfo) => {
      this.userHasCompany = !!userInfo.company;
      if (!this.userHasCompany) {
        window.dispatchEvent(
          new CustomEvent('vaadin-router-go', {
            detail: {
              pathname: '/settings/company'
            }
          })
        );
      }
      this.requestUpdate();
    });
  }

  public disconnectedCallback(): void {
    destroy(this._destroy$);
  }

  private _navigateTo = (where: string) => (): boolean => Router.go(where);
  private _isPath = (path: string): boolean => this.currentRoutePath.includes(path);
  private _isPathExact = (path: string): boolean => this.currentRoutePath === path;

  private _logout = () => (): Promise<void> => setIsNotLoggedIn();

  private _renderUserInfo() {
    return this._subscribe(
      getUserInfo$,
      (userInfo: UserInfo) => html`
        <div class="">
          <div class="main-menu-header">
            <img class="img-radius" src="${userInfo.avatarImgUrl}" alt="User-Profile-Image" />
            <div class="user-details">
              <div id="more-details" @click="${() => (this._userInfoExpanded = !this._userInfoExpanded)}">
                ${userInfo.email} <i class="fa fa-caret-down"></i>
              </div>
            </div>
          </div>
          <div
            class="collapse"
            id="nav-user-link"
            style=${styleMap(this._userInfoExpanded ? displayBlock : displayNone)}
          >
            <p class="text-center m-1"><i class="fa fa-user"></i> Lvl ${userInfo.userLevelInfo.level}</p>
            <p class="text-center">
              <i class="fa fa-book"></i> ${userInfo.experience} / ${userInfo.userLevelInfo.expToNextLevel} EXP
            </p>
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="user-profile.html" data-toggle="tooltip" title="View Profile"
                  ><i class="feather icon-user"></i
                ></a>
              </li>
              <li class="list-inline-item">
                <a href="email_inbox.html"
                  ><i class="feather icon-mail" data-toggle="tooltip" title="Messages"></i
                  ><small class="badge badge-pill badge-primary">5</small></a
                >
              </li>
              <li class="list-inline-item">
                <a @click="${this._logout()}" data-toggle="tooltip" title="Logout" class="text-danger"
                  ><i class="feather icon-power"></i
                ></a>
              </li>
            </ul>
          </div>
        </div>
      `
    );
  }

  public render(): TemplateResult {
    return html` <div class="navbar-wrapper  ">
      <div class="navbar-content scroll-div ">
        ${this._renderUserInfo()}
        ${this.userHasCompany
          ? html`<ul class="nav pcoded-inner-navbar ">
          <li class="nav-item pcoded-menu-caption">
            <label>Navigation</label>
          </li>
          <li class="nav-item ${this._isPathExact('/') ? 'pcoded-trigger' : ''}">
            <a class="nav-link " href="/"
              ><span class="pcoded-micon"><i class="feather icon-home"></i></span
              ><span class="pcoded-mtext">Dashboard</span></a
            >
          </li>

          <li
            class="nav-item pcoded-hasmenu ${this._isPath('/ranking') ? 'pcoded-trigger' : ''} ${
              this._isPath('/ranking') ? 'active' : ''
            }"
          >
            <a href="/ranking" class="nav-link has-ripple"
              ><span class="pcoded-micon"><i class="feather icon-list"></i></span
              ><span class="pcoded-mtext">Ranking</span
              ><span
                class="ripple ripple-animate"
                style="height: 210px; width: 210px; animation-duration: 0.7s; animation-timing-function: linear; background: rgb(255, 255, 255); opacity: 0.4; top: -87px; left: -34px;"
              ></span
            ></a>
            <ul class="pcoded-submenu">
              <li class="${this._isPathExact('/ranking') ? 'active' : ''}"><a href="/ranking">Users</a></li>
              <li class="${this._isPathExact('/ranking/teams') ? 'active' : ''}"><a href="/ranking/teams">Teams</a></li>
              <li class="${this._isPathExact('/ranking/companies') ? 'active' : ''}">
                <a href="/ranking/companies">Companies</a>
              </li>
            </ul>
          </li>
          <li class="nav-item ${this._isPath('/top') ? 'pcoded-trigger' : ''}">
            <a href="/top" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-star"></i></span
              ><span class="pcoded-mtext">TOP</span></a
            >
          </li>
          <li class="nav-item ${this._isPath('/statistics') ? 'pcoded-trigger' : ''}">
            <a href="/statistics" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-bar-chart"></i></span
              ><span class="pcoded-mtext">Statistics</span></a
            >
          </li>
          <li class="nav-item ${this._isPath('/compare') ? 'pcoded-trigger' : ''}">
            <a href="/compare" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-shuffle"></i></span
              ><span class="pcoded-mtext">Compare</span></a
            >
          </li>

          <li class="nav-item pcoded-menu-caption">
            <label>Game</label>
          </li>
          <li class="nav-item disabled">
            <a href="#!" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-calendar"></i></span
              ><span class="pcoded-mtext">Office</span></a
            >
          </li>
          <li class="nav-item disabled">
            <a href="#!" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-battery"></i></span
              ><span class="pcoded-mtext">Avatar</span></a
            >
          </li>
          <li class="nav-item disabled">
            <a href="#!" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-box"></i></span
              ><span class="pcoded-mtext">City</span></a
            >
          </li>

          <li class="nav-item pcoded-menu-caption">
            <label>Settings</label>
          </li>

          <li class="nav-item ${this._isPath('/settings/company') ? 'pcoded-trigger' : ''}">
            <a href="/settings/company" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-briefcase"></i></span
              ><span class="pcoded-mtext">Company</span></a
            >
          </li>

          <li class="nav-item disabled">
            <a href="#!" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-user"></i></span
              ><span class="pcoded-mtext">Profile</span></a
            >
          </li>
          <li class="nav-item disabled">
            <a href="#!" class="nav-link "
              ><span class="pcoded-micon"><i class="feather icon-users"></i></span
              ><span class="pcoded-mtext">Teams</span></a
            >
          </li>
          <li class="nav-item ${this._isPath('/settings/integrations') ? 'pcoded-trigger' : ''}"">
            <a href="/settings/integrations/" class="nav-link ">
              <span class="pcoded-micon"><i class="feather icon-codepen"></i></span
              ><span class="pcoded-mtext">Integrations</span>
              <span class="pcoded-badge badge badge-success">1</span>
              <span class="pcoded-badge badge badge-warning">3</span>
            </a>
          </li>
        </ul>`
          : html``}
      </div>
    </div>`;
  }
}

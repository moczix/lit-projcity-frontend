import '@webcomponents/webcomponentsjs/webcomponents-loader';
import { runAppInitializer, dashboardInitializer } from '@logic/app/app-initializer';

runAppInitializer();
dashboardInitializer();

import './dashboard.page';
import './pages/main-page/main.page';
import './pages/ranking/ranking-users.page';
import './pages/ranking/ranking-companies.page';
import './pages/ranking/ranking-teams.page';
import './pages/statistics/statistics.page';
import './pages/statistics/statistics-by-user.page';
import './pages/task-details/task-details.page';
import './pages/settings/settings-company-page';
import './pages/settings/settings-integrations-page';

// elements
import './elements/header.element';
import './elements/left-sidebar.element';
import './elements/user-info-panel.element';
import './pages/main-page/elements/latest-activity-panel.element';
import './pages/main-page/elements/actions-panels.element';
import './pages/statistics/elements/task-assigned-mini-panel.element';
import './pages/statistics/elements/task-in-progress-mini-panel.element';
import './pages/statistics/elements/pull-requests-open-mini-panel.element';
import './pages/statistics/elements/jira-statistics-today-mini-panel.element';
import './pages/statistics/elements/jira-statistics-week-mini-panel.element';
import './pages/statistics/elements/pull-request-statistics-today-mini-panel.element';
import './pages/statistics/elements/pull-request-statistics-week-mini-panel.element';

import './elements/tasks-panel.element';

import './../game/game.element';

import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Router } from '@vaadin/router';
import { subscribe, ifHtml } from '@helpers/lit-html';
import { authIsLoggedIn$ } from '@logic/auth/state-rx';
import { filter, take, delay } from 'rxjs/operators';

const _mainRouter = () => {
  let router: Router;
  return (outlet: HTMLElement | null): Router => {
    if (!outlet) {
      return router;
    }
    router = new Router(outlet);
    router.setRoutes([
      { path: '/', component: 'pc-main-page' },
      {
        path: '/statistics',
        children: [
          {
            path: '/',
            component: 'pc-statistics-page'
          },
          {
            path: '/:userId',
            component: 'pc-statistics-by-user-page'
          }
        ]
      },
      //{ path: '/ranking', component: 'pc-ranking-users-page' },
      {
        path: '/ranking',
        children: [
          {
            path: '/',
            component: 'pc-ranking-users-page'
          },
          {
            path: '/companies',
            component: 'pc-ranking-companies-page'
          },
          {
            path: '/teams',
            component: 'pc-ranking-teams-page'
          }
        ]
      },
      { path: '/top', component: 'pc-top-page' },
      { path: '/compare', component: 'pc-compare-page' },
      { path: '/task-details/:taskId', component: 'pc-task-details-page' },
      {
        path: '/settings',
        children: [
          {
            path: '/company',
            component: 'pc-settings-company-page'
          },
          {
            path: '/integrations',
            component: 'pc-settings-integrations-page'
          }
        ]
      }
    ]);
    return router;
  };
};
const setupRouter = _mainRouter();

export const getRouter = (): Router => setupRouter(null);

@customElement('pc-dashboard-page')
export class DashboardPage extends LitElementShadowless {
  firstUpdated(): void {
    authIsLoggedIn$
      .pipe(filter(Boolean), take(1), delay(100))
      .subscribe(() => setupRouter(document.getElementById('dashboard-router')));
  }

  public render(): TemplateResult {
    return html`
      ${subscribe(authIsLoggedIn$, (isLoggedIn: boolean) =>
        ifHtml(
          isLoggedIn,
          html`
            <pc-header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue"></pc-header>
            <pc-left-sidebar class="pcoded-navbar menu-light "></pc-left-sidebar>

            <div class="pcoded-main-container">
              <div class="pcoded-content">
                <div id="dashboard-router"></div>
              </div>
            </div>
          `
        )
      )}
    `;
  }
}

import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Subject } from 'rxjs';
import { subscribeCtor } from '@helpers/lit-html';
import { destroy } from '@logic/shared/helpers';
import { Experience } from '@logic/shared/models/experience.model';
import { getUserExperienceInfo$ } from '@logic/experience/state-rx';
import { repeat } from 'lit-html/directives/repeat';
import { formatDistance } from 'date-fns';
import { tap } from 'rxjs/operators';
@customElement('pc-latest-activity-panel')
export class LatestActivityPanelElement extends LitElementShadowless {
  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  public disconnectedCallback(): void {
    destroy(this._destroy$);
  }

  public render(): TemplateResult {
    return html`
      <div class="card feed-card">
        <div class="card-header">
          <h5>Latest Activity</h5>
        </div>
        <div class="latest-scroll" style="height:335px;position:relative;">
          <div class="card-body">
            ${this._subscribe(
              getUserExperienceInfo$,
              (experiences: Experience[]) => html`
                ${repeat(
                  experiences.slice(0, 5),
                  (experience) => experience.id,
                  (experience) => html`
                    <div class="row m-b-30">
                      <div class="col-auto p-r-0">
                        <i class="fab fa-${experience.integration.value}"></i>
                      </div>
                      <div class="col">
                        <a href="#!">
                          <h6 class="m-b-5">
                            ${experience.event.display} [+${experience.amount} exp]
                            <span class="text-muted float-right f-13"
                              >${formatDistance(experience.createdAt, new Date())}</span
                            >
                          </h6>
                        </a>
                      </div>
                    </div>
                  `
                )}
              `
            )}
          </div>
        </div>
        <div class="card-footer text-center">
          <a href="#!" class="b-b-primary text-primary">View all Feeds</a>
        </div>
      </div>
    `;
  }
}

import { customElement, html } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { Subject, combineLatest } from 'rxjs';
import { repeat } from 'lit-html/directives/repeat';
import { subscribeCtor } from '@helpers/lit-html';
import { destroy } from '@logic/shared/helpers';
import { getUserInfo$ } from '@logic/user/state-rx';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { LitDirectivePart } from '@helpers/types';
import { TopStats } from '@logic/shared/models/top-stats.model';
import { getTodayMyTopStats$, getMyRankPositionTopStats$ } from '@logic/top-stats/state-rx';
import { TopStatsMyRank } from '@logic/top-stats/types';
import { tap } from 'rxjs/operators';

@customElement('pc-actions-panels')
export class ActionsPanelsElement extends LitElementShadowless {
  private _destroy$ = new Subject<void>();
  private _subscribe = subscribeCtor(this._destroy$);

  public disconnectedCallback(): void {
    destroy(this._destroy$);
  }

  public render(): LitDirectivePart {
    return this._subscribe(
      combineLatest(getTodayMyTopStats$, getMyRankPositionTopStats$),
      ([topStats, rankStats]: [TopStats | undefined, TopStatsMyRank | undefined]) =>
        topStats
          ? html`
              <div class="row">
                <div class="col">
                  <div class="card">
                    <div class="card-body">
                      <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                          <i class="fab fa-jira f-30 text-c-purple"></i>
                        </div>
                        <div class="col-auto">
                          <h6 class="text-muted m-b-10">Jira actions</h6>
                          <h2 class="m-b-0">${topStats.jiraActions}</h2>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer bg-c-blue">
                      <div class="row align-items-center">
                        <div class="col-9">
                          <p class="text-white m-b-0">#${rankStats?.jiraActions} in ranking today</p>
                        </div>
                        <div class="col-3 text-right">
                          <i class="feather icon-trending-up text-white f-16"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="card">
                    <div class="card-body">
                      <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                          <i class="fab fa-gitlab f-30 text-c-green"></i>
                        </div>
                        <div class="col-auto">
                          <h6 class="text-muted m-b-10">Gitlab actions</h6>
                          <h2 class="m-b-0">${topStats.gitlabActions}</h2>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer bg-c-green">
                      <div class="row align-items-center">
                        <div class="col-9">
                          <p class="text-white m-b-0">#${rankStats?.gitlabActions} in ranking today</p>
                        </div>
                        <div class="col-3 text-right">
                          <i class="feather icon-trending-up text-white f-16"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="card">
                    <div class="card-body">
                      <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                          <i class="fab fa-slack f-30 text-c-red"></i>
                        </div>
                        <div class="col-auto">
                          <h6 class="text-muted m-b-10">Slack actions</h6>
                          <h2 class="m-b-0">${topStats.slackActions}</h2>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer bg-c-red">
                      <div class="row align-items-center">
                        <div class="col-9">
                          <p class="text-white m-b-0">#${rankStats?.slackActions} in ranking today</p>
                        </div>
                        <div class="col-3 text-right">
                          <i class="feather icon-trending-up text-white f-16"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col">
                  <div class="card">
                    <div class="card-body">
                      <div class="row align-items-center m-l-0">
                        <div class="col-auto">
                          <i class="fa fa-book f-30 text-c-yellow"></i>
                        </div>
                        <div class="col-auto">
                          <h6 class="text-muted m-b-10">Experience gained</h6>
                          <h2 class="m-b-0">${topStats.experience}</h2>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer bg-c-yellow">
                      <div class="row align-items-center">
                        <div class="col-9">
                          <p class="text-white m-b-0">#${rankStats?.experience} in ranking today</p>
                        </div>
                        <div class="col-3 text-right">
                          <i class="feather icon-trending-up text-white f-16"></i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            `
          : html``
    );
  }
}

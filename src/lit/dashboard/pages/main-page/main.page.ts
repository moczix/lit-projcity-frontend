import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-main-page')
export class MainPage extends LitElementShadowless {
  firstUpdated(): void {}

  public render(): TemplateResult {
    return html`
      <!-- [ breadcrumb ] start -->
      <div class="page-header">
        <div class="page-block">
          <div class="row align-items-center">
            <div class="col-md-12">
              <div class="page-header-title">
                <h5 class="m-b-10"><i class="feather icon-home"></i> Dashboard</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- [ breadcrumb ] end -->
      <!-- [ Main Content ] start -->
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <!-- support-section start -->
          <pc-actions-panels></pc-actions-panels>
          <!-- support-section end -->
        </div>
        <!-- prject ,team member start -->
        <div class="col-xl-8 col-md-6"><pc-tasks-panel listLimit="5"></pc-tasks-panel></div>
        <div class="col-xl-4 col-md-6">
          <pc-latest-activity-panel></pc-latest-activity-panel>
        </div>

        <!-- Latest Customers end -->
      </div>
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <pc-user-info-panel></pc-user-info-panel>
        </div>
        <div class="col-xl-8 col-md-6">
          <pc-game></pc-game>
        </div>
      </div>
    `;
  }
}

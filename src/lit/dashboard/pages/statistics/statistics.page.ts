import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-statistics-page')
export class StatisticsPage extends LitElementShadowless {
  firstUpdated(): void {}

  public render(): TemplateResult {
    return html`
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <pc-user-info-panel></pc-user-info-panel>
        </div>
        <div class="col-lg-8 col-md-6">
          <!-- support-section start -->
          <div class="row">
            <div class="col">
              <pc-task-assigned-mini-panel></pc-task-assigned-mini-panel>
            </div>
            <div class="col">
              <pc-task-in-progress-mini-panel></pc-task-in-progress-mini-panel>
            </div>
            <div class="col">
              <pc-pull-requests-mini-panel></pc-pull-requests-mini-panel>
            </div>
          </div>

          <div class="row">
            <div class="col-xl-6 col-md-6">
              <pc-jira-statistics-today-mini-panel></pc-jira-statistics-today-mini-panel>
            </div>
            <div class="col-xl-6 col-md-6">
              <pc-pull-request-today-mini-panel></pc-pull-request-today-mini-panel>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-6 col-md-6">
              <<pc-jira-statistics-week-mini-panel></pc-jira-statistics-week-mini-panel>
            </div>
            <div class="col-xl-6 col-md-6">
              <pc-pull-request-week-mini-panel></pc-pull-request-week-mini-panel>
            </div>
          </div>
          <!-- support-section end -->
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-md-12">
          <pc-tasks-panel listLimit="${Infinity}"></pc-tasks-panel>
        </div>
      </div>
    `;
  }
}

import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { getRouter } from '@lit-dashboard/dashboard.page';

@customElement('pc-statistics-by-user-page')
export class StatisticsByUserPage extends LitElementShadowless {
  @property({ type: Object }) location = getRouter().location;

  firstUpdated(): void {}

  public render(): TemplateResult {
    return html`
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <pc-user-info-panel userId="${this.location.params.userId}"></pc-user-info-panel>
        </div>
        <div class="col-lg-8 col-md-6">
          <!-- support-section start -->
          <div class="row">
            <div class="col">
              <pc-task-assigned-mini-panel userId="${this.location.params.userId}"></pc-task-assigned-mini-panel>
            </div>
            <div class="col">
              <pc-task-in-progress-mini-panel userId="${this.location.params.userId}"></pc-task-in-progress-mini-panel>
            </div>
            <div class="col">
              <pc-pull-requests-mini-panel userId="${this.location.params.userId}"></pc-pull-requests-mini-panel>
            </div>
          </div>

          <div class="row">
            <div class="col-xl-6 col-md-6">
              <pc-jira-statistics-today-mini-panel
                userId="${this.location.params.userId}"
              ></pc-jira-statistics-today-mini-panel>
            </div>
            <div class="col-xl-6 col-md-6">
              <pc-pull-request-today-mini-panel
                userId="${this.location.params.userId}"
              ></pc-pull-request-today-mini-panel>
            </div>
          </div>
          <div class="row">
            <div class="col-xl-6 col-md-6">
              <<pc-jira-statistics-week-mini-panel
                userId="${this.location.params.userId}"
              ></pc-jira-statistics-week-mini-panel>
            </div>
            <div class="col-xl-6 col-md-6">
              <pc-pull-request-week-mini-panel
                userId="${this.location.params.userId}"
              ></pc-pull-request-week-mini-panel>
            </div>
          </div>
          <!-- support-section end -->
        </div>
      </div>

      <div class="row">
        <div class="col-xl-12 col-md-12">
          <pc-tasks-panel listLimit="${Infinity}" userId="${this.location.params.userId}"></pc-tasks-panel>
        </div>
      </div>
    `;
  }
}

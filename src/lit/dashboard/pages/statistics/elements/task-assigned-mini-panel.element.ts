import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowless, LitElementShadowlessRx } from '@helpers/lit-element-shadowless';
import { TaskStatusType } from '@logic/shared/models/task';
import { getTasksStatusForUser } from '@logic/core/api/integrations';
import { getUserIdCold$ } from '@logic/user/state-rx';
import { TaskStatus } from '@logic/shared/models/task-status.model';
import { TopStats } from '@logic/shared/models/top-stats.model';
import { getTopPullRequestOpenToday } from '@logic/core/api/top-stats';
import { getAssignablesStatisticsOrderByTaskAssigned } from '@logic/core/api/avatar-assignable-statistics';
import * as R from 'rambdax';
import { AssignablesStatistics } from '@logic/shared/models/assignables-statistics.model';

@customElement('pc-task-assigned-mini-panel')
export class TaskAssignedMiniPanelElement extends LitElementShadowlessRx {
  @property({ type: Number }) public userId: number | undefined;

  public taskCount: number | undefined = 0;
  public ranking: number | string = 'NONE';

  firstUpdated(): void {
    this._runAwaiter();
  }

  private async _runAwaiter(): Promise<void> {
    if (!this.userId) {
      this.userId = await getUserIdCold$.toPromise();
    }

    const stats: AssignablesStatistics[] = await getAssignablesStatisticsOrderByTaskAssigned();
    this.taskCount = R.find(R.propEq('userId', this.userId), stats)?.countTaskOpened;
    this.ranking = R.findIndex(R.propEq('userId', this.userId), stats);
    if (this.ranking === -1) {
      this.ranking = 'NONE';
    } else {
      this.ranking++;
    }
    this.requestUpdate();
  }

  public render(): TemplateResult {
    return html`<div class="card">
      <div class="card-body">
        <div class="row align-items-center m-l-0">
          <div class="col-auto">
            <i class="fab fa-jira f-30 text-c-purple"></i>
          </div>
          <div class="col-auto">
            <h6 class="text-muted m-b-10">Task assigned</h6>
            <h2 class="m-b-0">${this.taskCount}</h2>
          </div>
        </div>
      </div>
      <div class="card-footer bg-c-blue">
        <div class="row align-items-center">
          <div class="col-9">
            <p class="text-white m-b-0">#${this.ranking} in global ranking</p>
          </div>
          <div class="col-3 text-right">
            <i class="feather icon-trending-up text-white f-16"></i>
          </div>
        </div>
      </div>
    </div>`;
  }
}

import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowlessRx } from '@helpers/lit-element-shadowless';
import { getAllIntegrationsPullRequestsForUser } from '@logic/core/api/integrations';
import { getUserIdCold$ } from '@logic/user/state-rx';
import { getTasksWithStatus } from '@logic/integrations/utils';
import { TaskStatusType } from '@logic/shared/models/task';
import { getUserExperienceBetweenDate } from '@logic/core/api/experience';
import { startOfDay, endOfDay } from 'date-fns';
import { getExperienceWithEvent } from '@logic/experience/utils';
import { ExperienceEventType } from '@logic/experience/enums';

@customElement('pc-pull-request-today-mini-panel')
export class PullRequestStatisticsTodayMiniPanelElement extends LitElementShadowlessRx {
  @property({ type: Number }) public userId: number | undefined;

  public createdCount = 0;
  public closedCount = 0;
  public commentsCount = 0;
  public commits = 0;

  firstUpdated(): void {
    this._runAwaiter();
  }

  private async _runAwaiter(): Promise<void> {
    if (!this.userId) {
      this.userId = await getUserIdCold$.toPromise();
    }

    const todayExp = await getUserExperienceBetweenDate(this.userId, startOfDay(new Date()), endOfDay(new Date()));
    this.createdCount = getExperienceWithEvent(ExperienceEventType.pullRequestCreated)(todayExp).length;
    this.closedCount = getExperienceWithEvent(ExperienceEventType.pullRequestClosed)(todayExp).length;
    this.commits = getExperienceWithEvent(ExperienceEventType.repoPush)(todayExp).length;
    this.requestUpdate();
  }

  public render(): TemplateResult {
    return html`<div class="card table-card widget-statstic-card">
      <div class="card-header borderless">
        <i class="fab fa-gitlab st-icon bg-c-green"></i>
        <span class="badge badge-info mr-2">today</span>

        <h5>Pull request statistics</h5>
      </div>
      <div class="card-body px-0 py-0">
        <div class="table-responsive">
          <div>
            <table class="table table-sm mb-0">
              <tbody>
                <tr>
                  <td>Created</td>
                  <td><h6>${this.createdCount}</h6></td>
                </tr>
                <tr>
                  <td>Closed</td>
                  <td><h6>${this.closedCount}</h6></td>
                </tr>
                <tr>
                  <td>Commits</td>
                  <td><h6>${this.commits}</h6></td>
                </tr>
                <tr>
                  <td>Comments</td>
                  <td><h6>${this.commentsCount}</h6></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>`;
  }
}

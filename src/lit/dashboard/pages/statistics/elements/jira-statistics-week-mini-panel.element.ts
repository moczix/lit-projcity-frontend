import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowlessRx } from '@helpers/lit-element-shadowless';
import { getAllIntegrationsPullRequestsForUser } from '@logic/core/api/integrations';
import { getUserIdCold$ } from '@logic/user/state-rx';
import { getTasksWithStatus } from '@logic/integrations/utils';
import { TaskStatusType } from '@logic/shared/models/task';
import { getUserExperienceBetweenDate } from '@logic/core/api/experience';
import { getExperienceWithEvent } from '@logic/experience/utils';
import { ExperienceEventType } from '@logic/experience/enums';
import { startOfWeek, endOfWeek } from 'date-fns';

@customElement('pc-jira-statistics-week-mini-panel')
export class JiraStatisticsWeekMiniPanelElement extends LitElementShadowlessRx {
  @property({ type: Number }) public userId: number | undefined;

  public taskCreatedCount = 0;
  public taskClosedCount = 0;
  public taskUpdatedCount = 0;
  public commentCreated = 0;

  firstUpdated(): void {
    this._runAwaiter();
  }

  private async _runAwaiter(): Promise<void> {
    if (!this.userId) {
      this.userId = await getUserIdCold$.toPromise();
    }
    const weekExp = await getUserExperienceBetweenDate(this.userId, startOfWeek(new Date()), endOfWeek(new Date()));
    this.taskCreatedCount = getExperienceWithEvent(ExperienceEventType.taskCreated)(weekExp).length;
    this.taskClosedCount = getExperienceWithEvent(ExperienceEventType.taskClosed)(weekExp).length;
    this.taskUpdatedCount = getExperienceWithEvent(ExperienceEventType.taskUpdated)(weekExp).length;
    this.commentCreated = getExperienceWithEvent(ExperienceEventType.commentCreated)(weekExp).length;
    this.requestUpdate();
  }

  public render(): TemplateResult {
    return html`<div class="card table-card widget-statstic-card">
      <div class="card-header borderless">
        <i class="fab fa-jira st-icon bg-c-blue"></i>
        <span class="badge badge-primary mr-2">Week</span>
        <h5 class="text-center">Jira statistics</h5>
      </div>
      <div class="card-body px-0 py-0">
        <div class="table-responsive">
          <div>
            <table class="table table-sm mb-0">
              <tbody>
                <tr>
                  <td>Created</td>
                  <td><h6>${this.taskCreatedCount}</h6></td>
                </tr>
                <tr>
                  <td>Closed</td>
                  <td><h6>${this.taskClosedCount}</h6></td>
                </tr>
                <tr>
                  <td>Updated</td>
                  <td><h6>${this.taskUpdatedCount}</h6></td>
                </tr>
                <tr>
                  <td>Comments</td>
                  <td><h6>${this.commentCreated}</h6></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>`;
  }
}

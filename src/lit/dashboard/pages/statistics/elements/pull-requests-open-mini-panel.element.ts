import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowlessRx } from '@helpers/lit-element-shadowless';
import { getAllIntegrationsPullRequestsForUser } from '@logic/core/api/integrations';
import { getUserIdCold$ } from '@logic/user/state-rx';
import { getTasksWithStatus } from '@logic/integrations/utils';
import { TaskStatusType } from '@logic/shared/models/task';
import { getTopPullRequestOpenToday } from '@logic/core/api/top-stats';
import { TopStats } from '@logic/shared/models/top-stats.model';
import * as R from 'rambdax';
import {
  getAssignablesStatisticsOrderByTaskAssigned,
  getAssignablesStatisticsOrderByPullRequestOpened
} from '@logic/core/api/avatar-assignable-statistics';
import { AssignablesStatistics } from '@logic/shared/models/assignables-statistics.model';

@customElement('pc-pull-requests-mini-panel')
export class PullRequestsMiniPanelElement extends LitElementShadowlessRx {
  @property({ type: Number }) public userId: number | undefined;

  public pullRequestsCount: number | undefined = 0;
  public ranking: string | number = 'NONE';
  firstUpdated(): void {
    this._runAwaiter();
  }

  private async _runAwaiter(): Promise<void> {
    if (!this.userId) {
      this.userId = await getUserIdCold$.toPromise();
    }

    const stats: AssignablesStatistics[] = await getAssignablesStatisticsOrderByPullRequestOpened();
    this.pullRequestsCount = R.find(R.propEq('userId', this.userId), stats)?.countPullRequestOpened;
    this.ranking = R.findIndex(R.propEq('userId', this.userId), stats) + 2;
    this.requestUpdate();
  }

  public render(): TemplateResult {
    return html`<div class="card">
      <div class="card-body">
        <div class="row align-items-center m-l-0">
          <div class="col-auto">
            <i class="fab fa-gitlab f-30 text-c-green"></i>
          </div>
          <div class="col-auto">
            <h6 class="text-muted m-b-10">Pull requests opened</h6>
            <h2 class="m-b-0">${this.pullRequestsCount}</h2>
          </div>
        </div>
      </div>
      <div class="card-footer bg-c-green">
        <div class="row align-items-center">
          <div class="col-9">
            <p class="text-white m-b-0">#${this.ranking} in ranking today</p>
          </div>
          <div class="col-3 text-right">
            <i class="feather icon-trending-up text-white f-16"></i>
          </div>
        </div>
      </div>
    </div>`;
  }
}

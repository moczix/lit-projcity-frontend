import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';

@customElement('pc-compare-page')
export class ComparePage extends LitElementShadowless {
  firstUpdated(): void {}

  public render(): TemplateResult {
    return html` stats page `;
  }
}

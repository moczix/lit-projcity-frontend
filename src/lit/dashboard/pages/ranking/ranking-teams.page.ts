import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { TopStatsWithUser } from '@logic/shared/models/top-stats.model';
import { repeat } from 'lit-html/directives/repeat';
import { getAvatarGroupStatistics, getAvatarGroupStatisticsByRangeDate } from '@logic/core/api/avatar-group-statistics';
import { AvatarGroupStatistics } from '@logic/shared/models/avatar-group-statistics.model';
import daterangepicker from 'daterangepicker';
import { format } from 'date-fns';
import { OrderBy, OrderByType } from './ranking-users.page';
import moment from 'moment';

enum Tabs {
  today,
  companies
}

@customElement('pc-ranking-teams-page')
export class RankingTeamsPage extends LitElementShadowless {
  public avatarGroupStats: AvatarGroupStatistics[] = [];
  private _orderBy = OrderBy.experience;
  public orderByType: OrderByType = OrderByType.desc;

  public daterange: daterangepicker | undefined;

  public startDate: Date = new Date();
  public endDate: Date = new Date();

  firstUpdated(): void {
    this._runAwaiter();
    const calendar: HTMLElement | null = this.querySelector('#reportrange');
    if (calendar) {
      this.daterange = new daterangepicker(
        calendar,
        {
          startDate: this.startDate,
          endDate: this.endDate,
          ranges: {
            Today: [moment(), moment()],
            Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        },
        (start, end, label) => {
          this.startDate = start.toDate();
          this.endDate = end.toDate();
          this._getRankingData();
          this.requestUpdate();
        }
      );
    }
  }

  private async _runAwaiter(): Promise<void> {
    this.avatarGroupStats = await getAvatarGroupStatistics();
    this.requestUpdate();
  }

  private async _getRankingData(): Promise<void> {
    this.avatarGroupStats = await getAvatarGroupStatisticsByRangeDate(this.startDate, this.endDate);
    this.requestUpdate();
  }

  public get companiesStatsOrderBy(): AvatarGroupStatistics[] {
    return this.avatarGroupStats
      .slice()
      .sort((a, b) =>
        this.orderByType === OrderByType.desc
          ? b.statistics[this._orderBy] - a.statistics[this._orderBy]
          : a.statistics[this._orderBy] - b.statistics[this._orderBy]
      );
  }

  public rangeDateFormat() {
    return `${format(this.startDate, 'LLL dd, yyyy')} - ${format(this.endDate, 'LLL dd, yyyy')}`;
  }

  public changeOrderByOrType(orderBy: OrderBy): void {
    return () => {
      if (orderBy === this._orderBy) {
        this.orderByType = this.orderByType === OrderByType.desc ? OrderByType.asc : OrderByType.desc;
      } else {
        this._orderBy = orderBy;
      }
      this.requestUpdate();
    };
  }

  public renderAvatarGroupStats(): TemplateResult {
    return html` <div class="card">
      <div class="card-header pb-0 mb-0">
        <div class="row align-items-center m-l-0">
          <div class="col-auto">
            <i class="fa fa-calendar f-30 text-c-purple"></i>
          </div>
          <div class="col-auto">
            <h6 class="text-muted m-b-10">Date range</h6>
            <form>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group fill">
                    <div class="input-group">
                      <div id="reportrange" class="form-control">
                        <h5><span class="ml-3">${this.rangeDateFormat()}</span> <i class="fa fa-caret-down"></i></h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="card-body table-border-style">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Team</th>
                <th>size</td>
                <th @click="${this.changeOrderByOrType(OrderBy.repositoryPush)}">
                  REPO PUSH
                  ${
                    this._orderBy === OrderBy.repositoryPush
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.pullRequestClosed)}">
                  PR closed
                  ${
                    this._orderBy === OrderBy.pullRequestClosed
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.pullRequestCreated)}">
                  PR opened
                  ${
                    this._orderBy === OrderBy.pullRequestCreated
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.issueCreated)}">
                  task created
                  ${
                    this._orderBy === OrderBy.issueCreated
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.issueClosed)}">
                  task closed
                  ${
                    this._orderBy === OrderBy.issueClosed
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.issueUpdated)}">
                  task updates
                  ${
                    this._orderBy === OrderBy.issueUpdated
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.experience)}">
                  Experience
                  ${
                    this._orderBy === OrderBy.experience
                      ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                      : html``
                  }
                </th>
              </tr>
            </thead>
            <tbody>
              ${repeat(
                this.companiesStatsOrderBy,
                (stat, index) => index,
                (stat: AvatarGroupStatistics, index) => html` <tr>
                  <td>${index + 1}</td>
                  <td>
                    <span class="badge badge-pill badge-success">${stat.groupName}</span>
                  </td>
                  <td>${stat.size}</td>
                  <td>${stat.statistics.repositoryPush}</td>
                  <td>${stat.statistics.pullRequestClosed}</td>
                  <td>${stat.statistics.pullRequestCreated}</td>
                  <td>${stat.statistics.issueCreated}</td>
                  <td>${stat.statistics.issueClosed}</td>
                  <td>${stat.statistics.issueUpdated}</td>
                  <td>${stat.statistics.experience}</td>
                </tr>`
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>`;
  }

  public render(): TemplateResult {
    return html`<div class="row">
      <div class="col-md-12">
        ${this.renderAvatarGroupStats()}
      </div>
    </div>`;
  }
}

import { customElement, html, TemplateResult } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { TopStatsWithUser } from '@logic/shared/models/top-stats.model';
import { getAllTopStatsWithUserToday, getAllTopStatsWithUserByDateRange } from '@logic/core/api/top-stats';
import daterangepicker from 'daterangepicker';
import { format } from 'date-fns';
import moment from 'moment';
import { repeat } from 'lit-html/directives/repeat';

export enum OrderBy {
  experience = 'experience',
  repositoryPush = 'repositoryPush',
  pullRequestClosed = 'pullRequestClosed',
  pullRequestCreated = 'pullRequestCreated',
  issueCreated = 'issueCreated',
  issueClosed = 'issueClosed',
  issueUpdated = 'issueUpdated'
}

export enum OrderByType {
  asc,
  desc
}

@customElement('pc-ranking-users-page')
export class RankingUsersPage extends LitElementShadowless {
  public allStats: TopStatsWithUser[] = [];
  private _orderBy = OrderBy.experience;
  public orderByType: OrderByType = OrderByType.desc;

  public daterange: daterangepicker | undefined;

  public startDate: Date = new Date();
  public endDate: Date = new Date();

  firstUpdated(): void {
    this._runAwaiter();
    const calendar: HTMLElement | null = this.querySelector('#reportrange');
    if (calendar) {
      this.daterange = new daterangepicker(
        calendar,
        {
          startDate: this.startDate,
          endDate: this.endDate,
          ranges: {
            Today: [moment(), moment()],
            Yesterday: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        },
        (start, end, label) => {
          this.startDate = start.toDate();
          this.endDate = end.toDate();
          this._getRankingData();
          this.requestUpdate();
        }
      );
    }
  }

  private async _runAwaiter(): Promise<void> {
    this.allStats = await getAllTopStatsWithUserToday();
    this.requestUpdate();
  }

  private async _getRankingData(): Promise<void> {
    this.allStats = await getAllTopStatsWithUserByDateRange(this.startDate, this.endDate);
    this.requestUpdate();
  }

  public get todayStatsOrderedBy(): TopStatsWithUser[] {
    return this.allStats
      .slice()
      .sort((a, b) =>
        this.orderByType === OrderByType.desc
          ? b[this._orderBy] - a[this._orderBy]
          : a[this._orderBy] - b[this._orderBy]
      );
  }

  public rangeDateFormat() {
    return `${format(this.startDate, 'LLL dd, yyyy')} - ${format(this.endDate, 'LLL dd, yyyy')}`;
  }

  public changeOrderByOrType(orderBy: OrderBy): void {
    return () => {
      if (orderBy === this._orderBy) {
        this.orderByType = this.orderByType === OrderByType.desc ? OrderByType.asc : OrderByType.desc;
      } else {
        this._orderBy = orderBy;
      }
      this.requestUpdate();
    };
  }

  public renderTodayStats(): TemplateResult {
    return html` <div class="card">
      <div class="card-header pb-0 mb-0">
        <div class="row align-items-center m-l-0">
          <div class="col-auto">
            <i class="fa fa-calendar f-30 text-c-purple"></i>
          </div>
          <div class="col-auto">
            <h6 class="text-muted m-b-10">Date range</h6>
            <form>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group fill">
                    <div class="input-group">
                      <div id="reportrange" class="form-control">
                        <h5><span class="ml-3">${this.rangeDateFormat()}</span> <i class="fa fa-caret-down"></i></h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="card-body table-border-style">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>User</th>
                <th>
                  Team
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.repositoryPush)}">
                  REPO PUSH
                  ${this._orderBy === OrderBy.repositoryPush
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.pullRequestClosed)}">
                  PR closed
                  ${this._orderBy === OrderBy.pullRequestClosed
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.pullRequestCreated)}">
                  PR opened
                  ${this._orderBy === OrderBy.pullRequestCreated
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.issueCreated)}">
                  task created
                  ${this._orderBy === OrderBy.issueCreated
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.issueClosed)}">
                  task closed
                  ${this._orderBy === OrderBy.issueClosed
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.issueUpdated)}">
                  task updates
                  ${this._orderBy === OrderBy.issueUpdated
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
                <th @click="${this.changeOrderByOrType(OrderBy.experience)}">
                  Experience
                  ${this._orderBy === OrderBy.experience
                    ? html`<i class="fa fa-caret-${this.orderByType === OrderByType.asc ? 'up' : 'down'}"></i>`
                    : html``}
                </th>
              </tr>
            </thead>
            <tbody>
              ${repeat(
                this.todayStatsOrderedBy,
                (stat, index) => index,
                (stat: TopStatsWithUser, index) => html` <tr>
                  <td>${index + 1}</td>
                  <td>
                    <a href="/statistics/${stat.user.id}">
                      <img
                        src="${stat.user.avatarImgUrl}"
                        alt="user-image"
                        class="img-radius wid-30 m-r-15 bg-c-blue"
                        data-toggle="tooltip"
                        data-placement="top"
                        title=""
                        data-original-title="Tooltip on top"
                      />
                      ${stat.user.username}
                    </a>
                  </td>
                  <td>
                    <span class="badge badge-pill badge-success">${stat.user?.group?.name}</span>
                  </td>
                  <td>${stat.repositoryPush}</td>
                  <td>${stat.pullRequestClosed}</td>
                  <td>${stat.pullRequestCreated}</td>
                  <td>${stat.issueCreated}</td>
                  <td>${stat.issueClosed}</td>
                  <td>${stat.issueUpdated}</td>
                  <td>${stat.experience}</td>
                </tr>`
              )}
            </tbody>
          </table>
        </div>
      </div>
    </div>`;
  }

  public render(): TemplateResult {
    return html`<div class="row">
      <div class="col-md-12">
        ${this.renderTodayStats()}
      </div>
    </div>`;
  }
}

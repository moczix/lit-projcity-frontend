import { customElement, html, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { joinCompanyByName, joinCompanyById, createCompany } from '@logic/core/api/company';
import { FetchError } from '@logic/core/http-client';
import { refreshUserInfo$, getUserInfo$ } from '@logic/user/state-rx';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { delay, take } from 'rxjs/operators';
import { getUserInfo } from '@logic/core/api/user-auth';

@customElement('pc-settings-company-page')
export class SettingsCompanyPage extends LitElementShadowless {
  public joinError: string | undefined;
  public createCompanyError: string | undefined;
  public userHasCompany: boolean | undefined;
  public companyName: string | undefined;

  firstUpdated(): void {
    getUserInfo$.pipe(take(1)).subscribe((userInfo: UserInfo) => {
      this.userHasCompany = !!userInfo.company;
      this.companyName = userInfo.company?.name;
      this.requestUpdate();
    });
  }

  public joinCompanySubmit() {
    return async (e: Event) => {
      e.preventDefault();
      const companyNameOrId: HTMLInputElement | null = this.querySelector('input[name="companyNameOrId"]');
      if (companyNameOrId) {
        try {
          if (isNaN(Number(companyNameOrId.value))) {
            await joinCompanyByName(companyNameOrId.value);
          } else {
            await joinCompanyById(Number(companyNameOrId.value));
          }
          this.userHasCompany = true;
          refreshUserInfo$.next(undefined);
          this.requestUpdate();
        } catch (err) {
          this.joinError = err?.error?.detail;
          this.requestUpdate();
        }
      }
    };
  }

  public createCompanySubmit() {
    return async (e: Event) => {
      e.preventDefault();
      const companyName: HTMLInputElement | null = this.querySelector('input[name="companyCreateName"]');
      if (companyName) {
        try {
          await createCompany(companyName?.value);
          refreshUserInfo$.next(undefined);
          this.userHasCompany = !!(await getUserInfo())?.company;
          this.requestUpdate();
        } catch (err) {
          this.createCompanyError = err?.error?.name?.[0];
          this.requestUpdate();
        }
      }
    };
  }

  private _renderCompanySettings() {
    if (this.userHasCompany === undefined) {
      return html``;
    }

    return html` ${this.userHasCompany
      ? html` <div class="col-md-6">
          <div class="card">
            <div class="card-body text-center">
              <i class="fas fa-briefcase text-c-blue d-block f-40"></i>
              <h4 class="m-t-20 m-b-20"><span class="text-c-blue">You joined</span> ${this.companyName}</h4>
            </div>
          </div>
        </div>`
      : html` <div class="col-md-6 col-lg-6">
            <div class="card">
              <div class="card-body text-center">
                <i class="fas fa-plus text-c-blue d-block f-40"></i>
                <h4 class="m-t-20 m-b-20"><span class="text-c-blue">+1</span> Create company</h4>
                <form @submit="${this.createCompanySubmit()}">
                  <div class="row justify-content-md-center">
                    <div class="col-md-4">
                      ${this.createCompanyError
                        ? html`<div class="alert alert-block alert-danger">${this.createCompanyError}</div>`
                        : html``}
                      <div id="div_id_name" class="form-group">
                        <label for="id_name" class=" requiredField"> Name </label>
                        <div class="">
                          <input
                            type="text"
                            name="companyCreateName"
                            maxlength="100"
                            class="textinput textInput form-control"
                            required=""
                          />
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <button class="btn btn-primary btn-sm btn-round" type="submit">Create</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-6">
            <div class="card">
              <div class="card-body text-center">
                <i class="fa fa-sign-in-alt text-c-green d-block f-40"></i>
                <h4 class="m-t-20 m-b-20"><span class="text-c-green">+1</span> Join company</h4>
                <form @submit="${this.joinCompanySubmit()}">
                  <div class="row justify-content-md-center">
                    <div class="col-md-4">
                      ${this.joinError
                        ? html`<div class="alert alert-block alert-danger">${this.joinError}</div>`
                        : html``}
                      <div id="div_id_name" class="form-group">
                        <label for="id_name" class=" requiredField">
                          Name or id
                        </label>
                        <div class="">
                          <input
                            type="text"
                            name="companyNameOrId"
                            maxlength="100"
                            class="textinput textInput form-control"
                            required=""
                          />
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <button class="btn btn-primary btn-sm btn-round" type="submit">Join</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>`}`;
  }

  render() {
    return html`<div class="row justify-content-md-center">
      ${this._renderCompanySettings()}
    </div>`;
  }
}

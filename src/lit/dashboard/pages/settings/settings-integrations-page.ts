import { customElement, html } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { getIntegrationsProfile, getIntegrationsProfileProjects } from '@logic/core/api/integrations';
import { IntegrationsProfile } from '@logic/shared/models/integrations-profile.model';
import { repeat } from 'lit-html/directives/repeat';
import { UserInfo } from '@logic/shared/models/user-info.model';
import { getUserInfo$ } from '@logic/user/state-rx';
import { take, filter } from 'rxjs/operators';
import { IntegrationsProfileProject } from '@logic/shared/models/integrations-profile-project.model';

@customElement('pc-settings-integrations-page')
export class SettingsIntegrationsPage extends LitElementShadowless {
  public integrationsProfile: IntegrationsProfile[] | undefined;
  public userInfo: UserInfo | undefined;
  public currentProjects: IntegrationsProfileProject[] | undefined;

  firstUpdated(): void {
    this._getData();
  }

  private async _getData() {
    this.integrationsProfile = await getIntegrationsProfile();
    this.userInfo = await getUserInfo$.pipe(filter<UserInfo>(Boolean), take(1)).toPromise();
    console.log('userinfo', this.userInfo);
    this.requestUpdate();
  }

  private loadProjects(valueName: string) {
    return async (ev: Event) => {
      const profile = this.integrationsProfile?.find((i) => i.providerValue === valueName);
      if (profile) {
        this.currentProjects = await getIntegrationsProfileProjects(profile.id);
        this.requestUpdate();
      }
    };
  }

  private cleanCurrentProjects() {
    return () => {
      this.currentProjects = undefined;
      this.requestUpdate();
    };
  }

  public renderConnectBoxed() {
    return html`<div class="row justify-content-md-center">
      <div
        class="modal fade bd-example-modal-lg"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myLargeModalLabel"
        data-backdrop="static"
        style="display: none;"
        aria-hidden="true"
      >
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title h4" id="myLargeModalLabel">
                <i class="fab fa-jira text-c-blue"></i> Add Jira project
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
                @click="${this.cleanCurrentProjects()}"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body table-border-style">
                      <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>ID</th>
                              <th>Actions</th>
                            </tr>
                          </thead>
                          <tbody>
                            ${this.currentProjects
                              ? repeat(
                                  this.currentProjects,
                                  (_, index) => index,
                                  (project: IntegrationsProfileProject, index: number) => html` <tr>
                                    <td>${index + 1}</td>
                                    <td>${project.name}</td>
                                    <td>${project.externalId}</td>
                                    <td>
                                      <button class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Add</button>
                                    </td>
                                  </tr>`
                                )
                              : html``}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      ${this.userInfo?.integrationsAvailable
        ? repeat(
            this.userInfo?.integrationsAvailable,
            (_, index) => index,
            (integration, index: number) => html` <div class="col-md-4 col-lg-4">
              <div class="card">
                <div class="card-body text-center">
                  <i class="fab fa-jira text-c-blue d-block f-40"></i>
                  <h4 class="m-t-20 m-b-20">
                    ${integration.name}
                    ${this.userInfo?.integrationsList?.includes(integration.name)
                      ? html`<p><i class="text-c-green">Connected</i></p>`
                      : html``}
                  </h4>

                  ${this.userInfo?.integrationsList?.includes(integration.name)
                    ? html` <div class="row justify-content-md-center">
                        <div class="col-md-12">
                          <a class="btn btn-danger btn-round" href="integrations.html">Disconnect</a>
                          <a
                            class="btn btn-info btn-round"
                            href="#"
                            data-toggle="modal"
                            data-target=".bd-example-modal-lg"
                            @click="${this.loadProjects(integration.name)}"
                            >Add project</a
                          >
                        </div>
                      </div>`
                    : html`
                        <div class="row justify-content-md-center">
                          <div class="col-md-12">
                            <a class="btn btn-primary btn-round" href="${integration.url}">Connect</a>
                          </div>
                        </div>
                      `}
                </div>
              </div>
            </div>`
          )
        : html``}
    </div>`;
  }

  public renderIntegrationsList() {
    return html`<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <h5><i class="fab fa-codepen"></i> Connected integrations</h5>
          </div>
          <div class="card-body table-border-style">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Integration</th>
                    <th>Name</th>
                    <th>Connected by</th>
                    <th>Date</th>
                    <th>Webhook id</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  ${this.integrationsProfile
                    ? repeat(
                        this.integrationsProfile,
                        (integration, index) => index,
                        (integration: IntegrationsProfile, index: number) => html` <tr>
                          <td>${index + 1}</td>
                          <td>
                            <i class="fab fa-${integration.providerValue} text-c-blue"></i>${integration.providerName}
                          </td>
                          <td>Projcity costam</td>
                          <td>${integration.username}</td>
                          <td>6.04.2020</td>
                          <td>${integration.webhookId}</td>
                          <td>
                            <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Remove</button>
                          </td>
                        </tr>`
                      )
                    : html``}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>`;
  }

  public render() {
    return html` ${this.renderConnectBoxed()} ${this.renderIntegrationsList()}`;
  }
}

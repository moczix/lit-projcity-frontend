import { customElement, html, TemplateResult, property } from 'lit-element';
import { LitElementShadowless } from '@helpers/lit-element-shadowless';
import { getRouter } from '@lit-dashboard/dashboard.page';
import { getTaskDetails, getTaskStatusForTaskId } from '@logic/core/api/integrations';
import { Task, TaskStatusType } from '@logic/shared/models/task';
import { TaskStatus } from '@logic/shared/models/task-status.model';
import { formatDistance, sub, format, intervalToDuration } from 'date-fns';
import * as R from 'rambdax';
import { repeat } from 'lit-html/directives/repeat';
import { UserMiniInfo } from '@logic/shared/models/user-info.model';
import * as chart from 'chart.js';
import 'chartjs-plugin-datalabels';

@customElement('pc-task-details-page')
export class TaskDetailsPage extends LitElementShadowless {
  @property({ type: Object }) location = getRouter().location;

  public task: Task | undefined;
  public taskStatuses: TaskStatus[] | undefined;
  public daysInProgressDate: Date | undefined;
  public lastStatusDate: Date | undefined;
  public participants: UserMiniInfo[] | undefined;
  public participantsLength: number | undefined;

  public uniqueTaskStatuses: TaskStatus[] | undefined;
  public taskStatusesDuration: { [status: number]: number } | undefined;
  public taskUsersDuration: { [userId: number]: number } | undefined;

  firstUpdated(): void {
    this._runAwaiter();
  }

  private async _runAwaiter(): Promise<void> {
    this.task = await getTaskDetails(Number(this.location.params.taskId));
    this.taskStatuses = R.sortBy(R.prop('createdAt'), await getTaskStatusForTaskId(this.task.id));
    this.daysInProgressDate = this.taskStatuses.find((t) => t.status.value === TaskStatusType.inProgress)?.createdAt;
    this.lastStatusDate = R.last(this.taskStatuses)?.createdAt;
    this.participants = R.piped(
      this.taskStatuses,
      R.map(R.prop('user')),
      R.uniqWith((one: UserMiniInfo, two: UserMiniInfo) => one.id === two.id)
    );
    this.participantsLength = R.uniqWith((one, two) => one.user?.id === two.user?.id, this.taskStatuses)?.length;
    this.uniqueTaskStatuses = R.uniqWith((one, two) => one.status.value === two.status.value, this.taskStatuses);
    this._calculateTaskStatusesDuration();
    this._calculateTaskUsersDuration();

    this._setupStatusesDurationPie();
    this._setupUsersDurationPie();
    this.requestUpdate();
  }

  private _calculateTaskStatusesDuration(): void {
    this.taskStatusesDuration = R.piped(this.taskStatuses, R.groupBy(R.path('status.value')), (grouped) =>
      R.piped(
        grouped,
        R.keys,
        R.map((key) =>
          R.piped(
            grouped[key],
            R.map((task: TaskStatus) => (task.endAt || new Date()).getTime() - task.createdAt.getTime()),
            R.map((calculated: number) => (calculated < 0 ? 0 : calculated)),
            R.reduce(
              (sum, calculated: number) => ({
                [key]: R.add(sum[key] || 0, calculated)
              }),
              {}
            )
          )
        ),
        R.reduce((acc, curr) => ({ ...acc, ...curr }), {})
      )
    );
  }

  private _calculateTaskUsersDuration(): void {
    this.taskUsersDuration = R.piped(this.taskStatuses, R.groupBy(R.path('user.id')), (grouped) =>
      R.piped(
        grouped,
        R.keys,
        R.map((key) =>
          R.piped(
            grouped[key],
            R.map((task: TaskStatus) => (task.endAt || new Date()).getTime() - task.createdAt.getTime()),
            R.map((calculated: number) => (calculated < 0 ? 0 : calculated)),
            R.reduce(
              (sum, calculated: number) => ({
                [key]: R.add(sum[key] || 0, calculated)
              }),
              {}
            )
          )
        ),
        R.reduce((acc, curr) => ({ ...acc, ...curr }), {})
      )
    );
  }

  public toDay(interval: number | undefined): number {
    if (!interval) {
      return 0;
    }
    return parseInt((interval / (60 * 60 * 24 * 1000)).toString(), 10);
  }

  private _piePercentOptions() {
    return {
      tooltips: {
        enabled: true
      },
      plugins: {
        datalabels: {
          formatter: (value, ctx) => {
            let sum = 0;
            const dataArr = ctx.chart.data.datasets[0].data;
            dataArr.map((data) => {
              sum += data;
            });
            console.log(sum);
            const percentage = ((value * 100) / sum).toFixed(2) + '%';
            return percentage;
          },
          color: '#fff'
        }
      }
    };
  }

  private _setupStatusesDurationPie(): void {
    const canvas = this.querySelector('#statusesDurationPie') as HTMLCanvasElement;
    const myPieChart = new chart.default.Chart(canvas, {
      type: 'pie',
      data: {
        datasets: [
          {
            data: this.uniqueTaskStatuses?.map((task) => this.toDay(this.taskStatusesDuration?.[task.status.value])),
            backgroundColor: ['red', 'blue', 'green', 'blue', 'red', 'blue']
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: this.uniqueTaskStatuses?.map((task) => task.status.display)
      },
      options: this._piePercentOptions()
    });
  }

  private _setupUsersDurationPie(): void {
    const canvas = this.querySelector('#usersDurationPie') as HTMLCanvasElement;
    const myPieChart = new chart.default.Chart(canvas, {
      type: 'pie',
      data: {
        datasets: [
          {
            data: this.participants?.map((user) => this.toDay(this.taskUsersDuration?.[user.id])),
            backgroundColor: ['red', 'blue', 'green', 'blue', 'red', 'blue']
          }
        ],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: this.participants?.map((user) => user.username)
      },
      options: this._piePercentOptions()
    });
  }

  public render(): TemplateResult {
    return html`
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fab fa-jira f-36 text-c-purple"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">${this.task?.title}</h6>
                  <h2 class="m-b-0">${this.task?.key}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-spinner f-36 text-c-yellow"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Status</h6>
                  <h2 class="m-b-0">${this.task?.status.title}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-user f-36 text-c-red"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Assigned to</h6>
                  <h2 class="m-b-0">${this.task?.status.currentUser?.username}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-arrow-up f-36 text-c-purple"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Priority</h6>
                  <h2 class="m-b-0">${this.task?.priority}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-tools f-36 text-c-yellow"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Days in progress</h6>
                  <h2 class="m-b-0">
                    ${this.daysInProgressDate
                      ? intervalToDuration({ start: new Date(), end: this.daysInProgressDate }).days
                      : ''}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-user-clock f-36 text-c-yellow"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Days in status</h6>
                  <h2 class="m-b-0">
                    ${this.lastStatusDate
                      ? intervalToDuration({ start: new Date(), end: this.lastStatusDate }).days
                      : ''}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-envelope f-36 text-c-red"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Reported by</h6>
                  <h2 class="m-b-0">${this.task?.reporter.username}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-calendar f-36 text-c-blue"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Created date</h6>
                  <h2 class="m-b-0">
                    ${this.taskStatuses?.[0]?.createdAt ? format(this.taskStatuses[0].createdAt, 'dd.MM.yyyy') : ''}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-clock f-36 text-c-yellow"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Days from date</h6>
                  <h2 class="m-b-0">
                    ${this.taskStatuses?.[0]?.createdAt
                      ? intervalToDuration({ start: new Date(), end: this.taskStatuses[0].createdAt }).days
                      : ''}
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-comment f-36 text-c-yellow"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Comments</h6>
                  <h2 class="m-b-0">${this.task?.comments}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-2 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-upload f-36 text-c-yellow"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Task updates</h6>
                  <h2 class="m-b-0">${this.task?.updates}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center m-l-0">
                <div class="col-auto">
                  <i class="fa fa-users f-36 text-c-red"></i>
                </div>
                <div class="col-auto">
                  <h6 class="text-muted m-b-10">Participants</h6>
                  <h2 class="m-b-0">${this.participantsLength}</h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xl-6 col-md-6">
          <div class="card table-card">
            <div class="card-header">
              <h5>Status activity</h5>
              <div class="card-header-right"></div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover table-borderless mb-0">
                  <thead>
                    <tr>
                      <th>Status</th>
                      <th>Avatar</th>
                      <th>Assigned</th>
                      <th>Date</th>
                      <th>Days in</th>
                    </tr>
                  </thead>
                  <tbody>
                    ${this.taskStatuses
                      ? repeat(
                          R.reverse(this.taskStatuses),
                          (t) => t.id,
                          (task) => html`
                            <tr>
                              <td>
                                <label
                                  class="badge ${task.status.value === TaskStatusType.open ? 'badge-info' : ''} ${task
                                    .status.value === TaskStatusType.inProgress
                                    ? 'badge-primary'
                                    : ''}
                                    ${task.status.value === TaskStatusType.merged ? 'badge-warning' : ''}
                                    
                                    "
                                  >${task.status.display}</label
                                >
                              </td>
                              <td>
                                ${task.user
                                  ? html`<a href="#! ">
                                      <img
                                        src="${task.user?.avatarImgUrl}"
                                        class="img-radius wid-50 align-top m-r-15 bg-c-blue"
                                    /></a>`
                                  : '-'}
                              </td>
                              <td>${task.user?.username ?? 'Unassigned'}</td>
                              <td>
                                ${format(task.createdAt, 'dd.MM.yyyy')}
                                <p class="text-muted">${format(task.createdAt, 'HH:mm')}</p>
                              </td>
                              <td>
                                ${intervalToDuration({ start: task.endAt ?? new Date(), end: task.createdAt }).days}
                              </td>
                            </tr>
                          `
                        )
                      : ''}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-6 col-md-6">
          <div class="card">
            <div class="card-header">
              <h5>Status chart in days</h5>
              <div class="card-header-right"></div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-6">
                  <canvas id="statusesDurationPie" width="100" height="100"></canvas>
                </div>
                <div class="col-6">
                  ${this.uniqueTaskStatuses
                    ? repeat(
                        this.uniqueTaskStatuses,
                        (task) => task.id,
                        (task) =>
                          html`<p>
                            ${task.status.display} : ${this.toDay(this.taskStatusesDuration?.[task.status.value])}
                          </p>`
                      )
                    : html``}
                </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header">
              <h5>Assignes chart</h5>
              <div class="card-header-right"></div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-6">
                  <canvas id="usersDurationPie" width="100" height="100"></canvas>
                </div>
                <div class="col-6">
                  ${this.participants
                    ? repeat(
                        this.participants,
                        (user) => user.id,
                        (user) => html`<p>${user.username} : ${this.toDay(this.taskUsersDuration?.[user.id])}</p>`
                      )
                    : html``}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }
}

import { Environment } from '@env/env';

export const env: Environment = {
  api: 'https://api.projcity.com',
  dashboardUrl: 'http://localhost:4300',
  dashboardUrlNew: 'http://localhost:4300',
  homePageUrl: 'http://localhost:4200'
};

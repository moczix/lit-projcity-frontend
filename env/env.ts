import { env as envDiffrent } from './env.prod';

export interface Environment {
  api: string;
  dashboardUrl: string;
  dashboardUrlNew: string;
  homePageUrl: string;
}

export const env = envDiffrent;

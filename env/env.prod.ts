import { Environment } from '@env/env';

export const env: Environment = {
  api: 'https://api.projcity.com',
  dashboardUrl: 'http://projcity-dashboard.s3-website.us-east-2.amazonaws.com',
  dashboardUrlNew: 'http://projcity-dashboard2.s3-website.eu-central-1.amazonaws.com',
  homePageUrl: 'http://projcity-home2.s3-website.eu-central-1.amazonaws.com'
};

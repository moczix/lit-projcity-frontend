import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import { terser } from "rollup-plugin-terser";
import copy from 'rollup-plugin-copy'
import replace from '@rollup/plugin-replace';
import serve from 'rollup-plugin-serve'
import livereload from 'rollup-plugin-livereload'
import commonjs from '@rollup/plugin-commonjs';

const isDev = process.argv.includes('--dev');
const isServe = process.argv.includes('--serve');

let defaultPort = 4200;
let defaultBuild = 'home';
let customCopy = null;
const runCmd = process.argv.find(c => c.includes('--run=')) || '--run=home'
if (runCmd) {
  const buildTarget = runCmd.split('=')[1];
  switch (buildTarget) {
    case 'home':
      defaultBuild = 'home';
      break;
    case 'dashboard':
      defaultBuild = 'dashboard';
      defaultPort = 4300;
      customCopy = {
        src: './assets', dest: 'dist'
      }
      break;
    case 'dashboard-new':
      defaultBuild = 'dashboard-new';
      defaultPort = 4300;
      break;
    case 'game':
      defaultBuild = 'game';
      defaultPort = 4300;
      break;
  }
}

export function plugins(indexPath, distName, port) {
  const copyTargets = [
    {
      src: indexPath, dest: 'dist', rename: 'index.html'
    },
  ];
  if (customCopy) {
    copyTargets.push(customCopy);
  }


  return [
    ...(isDev ? [replace({ './env.prod': './env.dev', delimiters: ['', ''] })] : []),
    resolve(),
    commonjs(),
    typescript(),
    ...(!isDev && !isServe ? [terser({
      output: {
        comments: false
      }
    }),] : []),
    copy({
      targets: copyTargets
    }),

    ...(isServe ? [serve({
      contentBase: 'dist',
      port: port,
      historyApiFallback: true
    }),      // index.html should be in root of project
    livereload()] : [])
  ];
}


export default {
  input: [`src/lit/${defaultBuild}/main.ts`],
  output: {
    //file: `dist/main.js`,
    dir: 'dist',
    format: 'es',
    sourcemap: true,

  },
  //inlineDynamicImports: true,
  manualChunks: (id) => {
    if (id.includes('node_modules')) {
      return 'vendor';
    }
  },
  plugins: plugins(`src/lit/${defaultBuild}/index.html`, 'home', defaultPort)
};
